package no.ntnu.imt3281.ludo.client;

import java.io.IOException;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.command.ServerCommandInvoker;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.common.LudoLogger;
import no.ntnu.imt3281.ludo.gui.ChallengeController;
import no.ntnu.imt3281.ludo.gui.GameBoardController;
import no.ntnu.imt3281.ludo.gui.LoginController;
import no.ntnu.imt3281.ludo.gui.LudoController;

/**
 * 
 * This is the main class for the client. **Note, change this to extend other
 * classes if desired.**
 * 
 * @author
 *
 */
public class Client extends Application {
    private static final Logger LOGGER = Logger.getLogger(Client.class.getName());
    
    public static final String HOSTNAME = "localhost";
    public static final int PORT = 65194;

    private Socket clientSocket;
    private ClientThread clientThread;
    private ChatManager chatManager;
    private LudoController ludoController;
    private ArrayList<GameBoardController> gameControllers;
    private ChallengeController challengeController;

    /**
     * Creates a client thread
     */
    public Client() {
        this.gameControllers = new ArrayList<>();
        this.chatManager = new ChatManager();
        this.clientSocket = this.connect();

        if (clientSocket != null) {
            this.clientThread = new ClientThread(this.clientSocket, new ServerCommandInvoker(this.chatManager, this));

            this.clientThread.setListener(e -> {
                LOGGER.log(Level.SEVERE, "Lost connection to server", e);
                Platform.exit();
            });

            this.clientThread.start();
        }
    }

    private Socket connect() {
        Socket socket = null;
        try {
            socket = new Socket(HOSTNAME, PORT);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Could not connect to server on " + HOSTNAME + " on port " + PORT
                    + ". Make sure the server is started.", e);
        }
        return socket;
    }

    /**
     * Returns whether the client is connected to a server or not
     * 
     * @return True if client connected
     */
    public boolean isConnected() {
        return this.clientSocket != null;
    }

    /**
     * Returns the chat manager object
     * 
     * @return The chat manager object
     */
    public ChatManager getChatManager() {
        return this.chatManager;
    }

    /**
     * Returns the client thread object
     * 
     * @return The client thread object
     */
    public ClientThread getClient() {
        return this.clientThread;
    }

    /**
     * Sets ludo controller
     * @param lc LudoController object
     */
    public void setLudoController(LudoController lc) {
        this.ludoController = lc;
    }

    /**
     * Returns ludo controller object
     * @return LudoController object
     */
    public LudoController getLudoController() {
        return this.ludoController;
    }

    /**
     * Adds a game controller object to the client
     * @param gbc GameBoardController object
     */
    public void addGameController(GameBoardController gbc) {
        this.gameControllers.add(gbc);
    }

    /**
     * Returns game controller objects
     * @return ArrayList of GameBoardController objects
     */
    public List<GameBoardController> getGameControllers() {
        return this.gameControllers;
    }

    /**
     * Sets the challenge controller object
     * @param cc ChallengeController object
     */
    public void setChallengeController(ChallengeController cc) {
        this.challengeController = cc;
    }

    /**
     * Returns the challenge controller object
     * @return ChallengeController object
     */
    public ChallengeController getChallengeController() {
        return this.challengeController;
    }

    /**
     * Starts JavaFX GUI
     */
    @Override
    public void start(Stage primaryStage) {
        try {
            ResourceBundle resourceUsed = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n");
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../gui/LoginDialog.fxml"));
            loader.setResources(resourceUsed);
            LoginController controller = new LoginController(this, resourceUsed);
            loader.setController(controller);

            AnchorPane root = loader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setTitle("Welcome");
            primaryStage.setResizable(false);
            primaryStage.sizeToScene();
            primaryStage.setOnCloseRequest(e -> this.close());

            primaryStage.show();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error starting JavaFX", e);
        }
    }

    /**
     * Closes the client by calling method close() in ClientThread object and
     * Platform.exit to close JavaFX
     */
    public void close() {
        if (this.clientThread != null) {
            this.clientThread.close();
        }
        Platform.exit();
    }

    /**
     * Main method launches JavaFX
     * 
     * @param args There are no arguments
     */
    public static void main(String[] args) {
        LudoLogger.setup("ClientLog.txt");
        launch(args);
    }
}
