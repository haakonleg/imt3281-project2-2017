package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * Command which is run when a client enters random game queue
 *
 */
public class ClientQueueRandomGame implements Command {

    GameManager gameManager;

    /**
     * Constructor
     * @param gm Reference to game manager object
     */
    public ClientQueueRandomGame(GameManager gm) {
        this.gameManager = gm;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        if (this.gameManager.addToRandomQueue(client.getUser().getName())) {
            client.sendResponse("ADDED");
        } else {
            client.sendResponse("NOT_ADDED");
        }

    }

}
