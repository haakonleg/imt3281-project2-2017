package no.ntnu.imt3281.ludo.command;

import javafx.application.Platform;

import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * Command which is run on server when a game is started
 *
 */
public class ServerStartGame implements Command {

    private Client client;

    /**
     * Constructor
     * @param c Reference to client object
     */
    public ServerStartGame(Client c) {
        this.client = c;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String red = params[0];
        String blue = params[1];
        String yellow = params[2];
        String green = params[3];
        String id = params[4];

        Platform.runLater(() -> this.client.getLudoController().startRandomGame(red, blue, yellow, green, id));

    }

}
