package no.ntnu.imt3281.ludo.command;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.PasswordHash;

/**
 * Lets a client create a new user. Does some checks and inserts the username
 * and hashed password into the database. If everything was okay a response "OK"
 * is sent to the client.
 * 
 * @author Hakkon
 *
 */
public class ClientNewUser implements Command {
    private static final Logger LOGGER = Logger.getLogger(ClientNewUser.class.getName());
    
    private Connection db;

    /**
     * Constructor
     * 
     * @param db
     *            A connection to the database.
     */
    public ClientNewUser(Connection db) {
        this.db = db;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String username = params[0];
        String password = params[1];

        if (username == null || username.length() < 3 || username.length() > 20) {
            client.sendResponse("INVALID_USERNAME");
            return;
        }

        if (password == null || password.length() < 3) {
            client.sendResponse("INVALID_PASSWORD");
            return;
        }

        PasswordHash hasher = new PasswordHash();
        String passwordHash = hasher.getHashedPassword(password);

        String sql = "INSERT INTO User (username, passwordHash) VALUES (?, ?)";
        try (PreparedStatement stmt = this.db.prepareStatement(sql)) {

            stmt.setString(1, username);
            stmt.setString(2, passwordHash);

            stmt.executeUpdate();
            client.sendResponse("OK");

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "SQLException in ClientNewUser", e);
            client.sendResponse("DATABASE_ERROR");
        }

    }
}
