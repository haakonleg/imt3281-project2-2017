package no.ntnu.imt3281.ludo.command;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.common.RememberMeToken;
import no.ntnu.imt3281.ludo.common.User;
import no.ntnu.imt3281.ludo.server.ClientManager;
import no.ntnu.imt3281.ludo.server.PasswordHash;

/**
 * Lets a client log in. The user is fetched from the database and server checks
 * if the hash of the password from user is equal to the hashed password stored
 * in the database. If so, a user object is associated with the client and also
 * sent back to the client in JSON form.
 * 
 * @author Hakkon
 *
 */
public class ClientLogin implements Command {
    private static final Logger LOGGER = Logger.getLogger(ClientLogin.class.getName());
    
    private Connection db;
    private ClientManager clientManager;
    private PasswordHash hasher;
    private Gson gson;

    /**
     * Constructor
     * 
     * @param db
     *            A connection to the database
     * @param clientManager
     *            A reference to the ClientManager object
     */
    public ClientLogin(Connection db, ClientManager clientManager) {
        this.db = db;
        this.clientManager = clientManager;
        this.hasher = new PasswordHash();
        this.gson = new Gson();
    }

    // Removes nested try block
    private PreparedStatement preparedStatementLogin(String username) throws SQLException {
        String sql = "SELECT pid, username, passwordHash FROM User WHERE username = ?";
        PreparedStatement stmt = this.db.prepareStatement(sql);
        stmt.setString(1, username);
        return stmt;
    }

    // Removes nested try block
    private PreparedStatement preparedStatementToken(int pid) throws SQLException {
        String sql = "SELECT pid, expires, token FROM UserToken WHERE pid = ?";
        PreparedStatement stmt = this.db.prepareStatement(sql);
        stmt.setInt(1, pid);
        return stmt;
    }

    private boolean loginWithPassword(ClientThread client, String username, String password, String passwordHash,
            int pid) {
        if (this.hasher.getHashedPassword(password).equals(passwordHash)) {
            // Create new user object, set it to the client, and return the user object to
            // client
            User user = new User(pid, username);
            client.setUser(user);
            return true;
        } else {
            return false;
        }
    }

    // Get "remember me" token
    private RememberMeToken getToken(int pid, String username) {
        try (PreparedStatement stmt = this.preparedStatementToken(pid); ResultSet result = stmt.executeQuery()) {
            if (!result.next()) {
                return null;
            }
            return new RememberMeToken(result.getInt(1), username, result.getLong(2), result.getString(3));
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "SQLException while getting token", e);
            return null;
        }
    }

    // Issue a "remember me" token
    private void issueToken(ClientThread client, int pid, String username) {
        // New token valid for 48 hours
        RememberMeToken newToken = RememberMeToken.newToken(pid, username, 48);

        String sql = "MERGE INTO UserToken (pid, expires, token) VALUES(?, ?, ?)";
        try (PreparedStatement stmt = this.db.prepareStatement(sql)) {
            stmt.setInt(1, newToken.getUser());
            stmt.setLong(2, newToken.getExpiration());
            // Hash token before inserting
            stmt.setString(3, hasher.getHashedPassword(newToken.getToken()));
            stmt.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQLException while issuing token", e);
        }

        String jsonToken = this.gson.toJson(newToken, RememberMeToken.class);
        client.sendResponse(jsonToken);
    }

    private boolean loginWithToken(ClientThread client, RememberMeToken token, String clientToken) {
        if (token == null) {
            return false;
        }

        if (token.hasExpired()) {
            return false;
        }

        if (!this.hasher.getHashedPassword(clientToken).equals(token.getToken())) {
            return false;
        }

        User user = new User(token.getUser(), token.getUsername());
        client.setUser(user);
        return true;
    }

    private void deleteToken(int pid) {
        String sql = "DELETE FROM UserToken WHERE pid = ?";
        try (PreparedStatement stmt = this.db.prepareStatement(sql)) {
            stmt.setInt(1, pid);
            stmt.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "SQLException on deleteToken", e);
        }
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String username = params[0];
        // password field also used as token
        String password = params[1];
        boolean rememberme = Boolean.parseBoolean(params[2]);

        if (clientManager.getClient(username) != null) {
            client.sendResponse("ALREADY_LOGGED_IN");
            return;
        }

        // Get user
        try (PreparedStatement stmt = this.preparedStatementLogin(username); ResultSet result = stmt.executeQuery()) {

            // Check if the user name is in the table
            if (!result.next()) {
                client.sendResponse("USER_NOT_FOUND");
                return;
            }

            // User from result
            int userPID = result.getInt(1);
            String userName = result.getString(2);
            String passwordHash = result.getString(3);

            // Get token
            RememberMeToken token = this.getToken(userPID, userName);

            // Delete token if not remember me anymore
            if (token != null && !rememberme) {
                this.deleteToken(userPID);
                token = null;
            }

            // Login with token
            if (this.loginWithToken(client, token, password)) {
                this.issueToken(client, userPID, userName);
            } else if (token != null) {
                client.sendResponse("INVALID_TOKEN");
                this.deleteToken(userPID);
                return;
            } else if (loginWithPassword(client, userName, password, passwordHash, userPID)) {
                if (rememberme) {
                    this.issueToken(client, userPID, userName);
                } else {
                    client.sendResponse("OK");
                }
            } else {
                client.sendResponse("INVALID_PASSWORD");
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "SQLException", e);
        }
    }
}
