package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * Command which is run when a game challenge is started
 *
 */
public class ClientChallengeStart implements Command {

    private GameManager gameManager;

    /**
     * Constructor
     * @param gm Reference to game manager object
     */
    public ClientChallengeStart(GameManager gm) {
        this.gameManager = gm;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        this.gameManager.startChallenge(client);

    }

}
