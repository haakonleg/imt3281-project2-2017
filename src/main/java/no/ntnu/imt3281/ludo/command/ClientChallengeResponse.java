package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * Command to send a response to a client on a game challenge request
 *
 */
public class ClientChallengeResponse implements Command {
    private GameManager gameManager;

    /**
     * Constructor
     * @param gm Reference to game manager object
     */
    public ClientChallengeResponse(GameManager gm) {
        this.gameManager = gm;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String playerName = params[0];
        String response = params[1];

        this.gameManager.challengePlayerReceive(client, playerName, response);

    }
}
