package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * Command used by a client to challenge another client to a game
 */
public class ClientChallengePlayer implements Command {
    private GameManager gameManager;

    /**
     * Constructor
     * @param gm Reference to game manager object
     */
    public ClientChallengePlayer(GameManager gm) {
        this.gameManager = gm;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String playerName = params[0];

        this.gameManager.challengePlayerSend(client, playerName);

    }
}
