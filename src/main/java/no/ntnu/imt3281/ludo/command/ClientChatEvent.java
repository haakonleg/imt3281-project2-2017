package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.Chat;

import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.ClientManager;

import com.google.gson.Gson;

/**
 * Is run when a client joins a chat room. If successful, the chat object is
 * sent back to the client in JSON form
 * 
 * @author Hakkon
 *
 */
public class ClientChatEvent implements Command {
    ChatManager chatManager;
    ClientManager clientManager;
    Gson gson;

    /**
     * Constructor
     * 
     * @param chatManager
     *            A reference to the ChatManager object to get chat objects
     * @param clientManager
     *            A reference to the ClientManager object to get client objects
     */
    public ClientChatEvent(ChatManager chatManager, ClientManager clientManager) {
        this.chatManager = chatManager;
        this.clientManager = clientManager;
        this.gson = new Gson();
    }
    
    private void joinChat(ClientThread client, Chat chat) {
        if (chat.hasUser(client.getUser().getName())) {
            client.sendResponse("ALREADY_JOINED");
            return;
        }

        // Add the client user to this chat
        chat.addUser(client.getUser().getName());
        // Return the chat object in JSON format to client
        client.sendResponse(gson.toJson(chat));
    }
    
    private void leaveChat(ClientThread client, Chat chat) {
        if (!chat.hasUser(client.getUser().getName())) {
            client.sendResponse("NOT_IN_CHAT");
            return;
        }
        
        // Remove client from chat
        chat.removeUser(client.getUser().getName());
        client.sendResponse("OK");
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String chatID = params[0];
        String action = params[1];

        if (client.getUser() == null) {
            client.sendResponse("NOT_LOGGED_IN");
            return;
        }

        // Get the chat room
        Chat tempChat = null;
        if ("GlobalChat".equals(chatID)) {
            tempChat = this.chatManager.getChat(0);
        } else {
            tempChat = this.chatManager.getChat(chatID);
            if (tempChat == null) {
                client.sendResponse("NO_SUCH_CHAT");
                return;
            }
        }

        if ("JOIN".equals(action)) {
            this.joinChat(client, tempChat);
        } else if ("LEAVE".equals(action)) {
            this.leaveChat(client, tempChat);
        }

        // Notify all other clients in chat
        for (int i = 0; i < tempChat.nrOfUsers(); i++) {
            String usrName = tempChat.getUser(i);
            ClientThread notify = this.clientManager.getClient(usrName);
            if (notify != null && notify != client) {
                notify.sendCommand(
                        "ChatUsrEvent&" + tempChat.getID() + "&" + client.getUser().getName() + "&" + action);
            }
        }

    }

}
