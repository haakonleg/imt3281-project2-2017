package no.ntnu.imt3281.ludo.command;

import java.util.ArrayList;

import com.google.gson.Gson;

import javafx.application.Platform;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.gui.GameBoardController;
import no.ntnu.imt3281.ludo.logic.PieceEvent;

/**
 * Command which is run on server when a piece is moved by client
 *
 */
public class ServerMoveEvent implements Command {

    private Client client;
    private Gson gson;

    /**
     * Constructor
     * @param c Reference to client object
     */
    public ServerMoveEvent(Client c) {
        this.client = c;
        this.gson = new Gson();
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        PieceEvent lastEvent = this.gson.fromJson(params[0], PieceEvent.class);
        String id = params[1];
        ArrayList<GameBoardController> controllers = (ArrayList<GameBoardController>) this.client.getGameControllers();

        for (int i = 0; i < controllers.size(); i++) {
            int temp = i;
            if (controllers.get(i).getGameID().equals(id)) {
                Platform.runLater(() -> controllers.get(temp).movePiece(lastEvent));
            }
        }
    }

}
