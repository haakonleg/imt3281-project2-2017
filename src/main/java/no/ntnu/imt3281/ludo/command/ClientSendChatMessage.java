package no.ntnu.imt3281.ludo.command;

import com.google.gson.Gson;

import no.ntnu.imt3281.ludo.common.Chat;

import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ChatMessage;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.ClientManager;

/**
 * Lets a client send a chat message. If everything was okay a response "OK" is
 * sent back to the client. Additionally, every client in the current chat is
 * notified about the new chat message.
 * 
 * @author Hakkon
 *
 */
public class ClientSendChatMessage implements Command {
    ChatManager chatManager;
    ClientManager clientManager;
    Gson gson;

    /**
     * Constructor
     * @param chatManager A reference to the ChatManager object
     * @param clientManager A reference to the ClientManager object
     */
    public ClientSendChatMessage(ChatManager chatManager, ClientManager clientManager) {
        this.chatManager = chatManager;
        this.clientManager = clientManager;
        this.gson = new Gson();
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String chatID = params[0];
        String chatMessage = params[1];

        if (client.getUser() == null) {
            client.sendResponse("NOT_LOGGED_IN");
            return;
        }

        // Get the chat room
        Chat tempChat = this.chatManager.getChat(chatID);

        if (tempChat == null) {
            client.sendResponse("NO_SUCH_CHAT");
            return;
        }

        // Try to send the chat message
        if (tempChat.sendMessage(client.getUser().getName(), chatMessage)) {
            client.sendResponse("OK");

            // Notify everyone in the chat
            ChatMessage sentMsg = new ChatMessage(client.getUser().getName(), chatMessage);

            for (int i = 0; i < tempChat.nrOfUsers(); i++) {
                String usrName = tempChat.getUser(i);
                ClientThread notify = clientManager.getClient(usrName);
                if (notify != null) {
                    notify.sendCommand("ChatMsgSent&" + tempChat.getID() + "&" + gson.toJson(sentMsg));
                }
            }

        } else {
            client.sendResponse("NOT_IN_CHAT");
        }
    }

}
