package no.ntnu.imt3281.ludo.command;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

import no.ntnu.imt3281.ludo.common.Chat;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ChatMessage;
import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * Lets the server notify clients about a new chat message.
 * 
 * @author Hakkon
 *
 */
public class ServerChatMessageSent implements Command {
    private static final Logger LOGGER = Logger.getLogger(ServerChatMessageSent.class.getName());
    
    private ChatManager chatManager;
    private Gson gson;

    /**
     * Constructor
     * @param manager A reference to the ChatManager object
     */
    public ServerChatMessageSent(ChatManager manager) {
        this.chatManager = manager;
        this.gson = new Gson();
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String chatID = params[0];
        String chatMsgJSON = params[1];

        Chat tempChat = chatManager.getChat(chatID);
        if (tempChat == null) {
            LOGGER.log(Level.WARNING, "Chat not found: " + chatID);
            return;
        }

        ChatMessage msg = gson.fromJson(chatMsgJSON, ChatMessage.class);

        tempChat.sendMessage(msg.getUsername(), msg.getMessage());
    }
}
