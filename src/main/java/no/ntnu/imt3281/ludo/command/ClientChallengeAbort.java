package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.GameManager;
/**
 * Abort a game challenge
 *
 */
public class ClientChallengeAbort implements Command {
    private GameManager gameManager;

    /**
     * Constructor
     * @param gm Reference to game manager object
     */
    public ClientChallengeAbort(GameManager gm) {
        this.gameManager = gm;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        this.gameManager.challengeAbort(client);
    }
}
