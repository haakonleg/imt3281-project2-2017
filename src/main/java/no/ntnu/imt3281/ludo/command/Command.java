package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * This is the interface for a ClientCommand, commands that are sent from the
 * client to server
 * 
 * @author Hakkon
 *
 */
public interface Command {
    /**
     * This method performs the command action
     * 
     * @param client
     *            A reference to the client which performed the action
     * @param params
     *            Parameters to pass to the execute method
     */
    void execute(ClientThread client, String[] params);
}
