package no.ntnu.imt3281.ludo.command;

import java.util.ArrayList;

import com.google.gson.Gson;

import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.gui.GameBoardController;
import no.ntnu.imt3281.ludo.logic.DiceEvent;

/**
 * Command which is run on server when a dice is thrown by a client
 *
 */
public class ServerDiceEvent implements Command {

    private Client client;
    private Gson gson;

    /**
     * Constructor
     * @param c Reference to client object
     */
    public ServerDiceEvent(Client c) {
        this.client = c;
        this.gson = new Gson();
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        DiceEvent lastEvent = this.gson.fromJson(params[0], DiceEvent.class);
        String id = params[1];
        ArrayList<GameBoardController> controllers = (ArrayList<GameBoardController>) this.client.getGameControllers();

        for (int i = 0; i < controllers.size(); i++) {
            if (controllers.get(i).getGameID().equals(id)) {
                controllers.get(i).updateDice(lastEvent);
            }
        }

    }

}
