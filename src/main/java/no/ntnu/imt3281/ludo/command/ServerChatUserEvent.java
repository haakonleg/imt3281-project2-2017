package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.Chat;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * Lets the server notify clients about a user event in a chat (when a user
 * joins or leaves).
 * 
 * @author Hakkon
 *
 */
public class ServerChatUserEvent implements Command {
    private ChatManager chatManager;

    /**
     * Constructor
     * @param manager A reference to the ChatManager object
     */
    public ServerChatUserEvent(ChatManager manager) {
        this.chatManager = manager;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String chatID = params[0];
        String username = params[1];
        String status = params[2];

        Chat tempChat = chatManager.getChat(chatID);
        if (tempChat == null) {
            return;
        }

        if ("JOIN".equals(status)) {
            tempChat.addUser(username);
        } else if ("LEAVE".equals(status)) {
            tempChat.removeUser(username);
        }
    }

}
