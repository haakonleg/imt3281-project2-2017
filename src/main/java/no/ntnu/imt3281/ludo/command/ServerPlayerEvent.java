package no.ntnu.imt3281.ludo.command;

import java.util.ArrayList;

import com.google.gson.Gson;

import javafx.application.Platform;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.gui.GameBoardController;
import no.ntnu.imt3281.ludo.logic.PlayerEvent;

/**
 * Command which is run on server on a player event
 *
 */
public class ServerPlayerEvent implements Command {

    private Client client;
    private Gson gson;

    /**
     * Constructor
     * @param c Reference to client object
     */
    public ServerPlayerEvent(Client c) {
        this.client = c;
        this.gson = new Gson();
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        PlayerEvent lastEvent = this.gson.fromJson(params[0], PlayerEvent.class);
        String id = params[1];
        ArrayList<GameBoardController> controllers = (ArrayList<GameBoardController>) this.client.getGameControllers();

        for (int i = 0; i < controllers.size(); i++) {
            int temp = i;
            if (controllers.get(i).getGameID().equals(id)) {
                Platform.runLater(() -> controllers.get(temp).playerStatusChange(lastEvent));
            }
        }
    }

}
