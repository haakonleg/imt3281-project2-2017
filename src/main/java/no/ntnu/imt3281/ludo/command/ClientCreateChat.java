package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.Chat;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * Command that is used when a client creates a new chat room.
 * @author Hakkon
 *
 */
public class ClientCreateChat implements Command {
    private ChatManager chatManager;

    /**
     * Constructor
     * @param chatManager A reference to the ChatManager object
     */
    public ClientCreateChat(ChatManager chatManager) {
        this.chatManager = chatManager;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String chatName = params[0];

        if (client.getUser() == null) {
            client.sendResponse("NOT_LOGGED_IN");
            return;
        }

        if (chatName.length() < 3) {
            client.sendResponse("INVALID_CHAT_NAME");
            return;
        }

        Chat newChat = new Chat(chatName);
        String newChatID = this.chatManager.addChat(newChat);
        client.sendResponse(newChatID);
    }

}
