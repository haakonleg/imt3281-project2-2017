package no.ntnu.imt3281.ludo.command;

import javafx.application.Platform;

import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * Command which is run when a game challenge is issued
 *
 */
public class ServerChallengeIssued implements Command {

    private Client client;

    /**
     * Constructor
     * @param c Reference to client object
     */
    public ServerChallengeIssued(Client c) {
        this.client = c;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        Platform.runLater(() -> this.client.getLudoController().challengeIssued(params[0]));
    }

}
