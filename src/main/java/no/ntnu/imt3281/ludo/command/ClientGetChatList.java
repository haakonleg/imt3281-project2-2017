package no.ntnu.imt3281.ludo.command;

import java.util.ArrayList;

import com.google.gson.Gson;

import no.ntnu.imt3281.ludo.common.Chat;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.gui.ChatListTable;

/**
 * Command that is used when the client request a list of all chat rooms.
 * @author Hakkon
 *
 */
public class ClientGetChatList implements Command {

    private ChatManager chatManager;
    private Gson gson;

    /**
     * Constructor
     * @param chatManager A reference to the ChatManager object
     */
    public ClientGetChatList(ChatManager chatManager) {
        this.chatManager = chatManager;
        this.gson = new Gson();
    }

    @Override
    public void execute(ClientThread client, String[] params) {

        if (client.getUser() == null) {
            client.sendResponse("NOT_LOGGED_IN");
            return;
        }

        // Send a table of all chat rooms
        ArrayList<ChatListTable> chatTables = new ArrayList<>();

        for (int i = 0; i < chatManager.nrOfChats(); i++) {
            Chat temp = chatManager.getChat(i);
            ChatListTable row = new ChatListTable(temp.getID(), temp.getName(), temp.nrOfUsers(), temp.getTimestamp());
            chatTables.add(row);
        }

        client.sendResponse(gson.toJson(chatTables));
    }

}
