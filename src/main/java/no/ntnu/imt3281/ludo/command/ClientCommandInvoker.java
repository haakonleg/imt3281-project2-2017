package no.ntnu.imt3281.ludo.command;

import java.sql.Connection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Logger;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.ClientManager;
import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * This is the invoker class, responsible for receiving commands from the client
 * and executing them. A hashmap is used with a string and a ClientCommand
 * object. When the server receives commands this invoker parses the command
 * string and executes the correct ClientCommand object with parameters.
 * 
 * @author Hakkon
 *
 */
public class ClientCommandInvoker implements Command {
    private static final Logger LOGGER = Logger.getLogger(ClientCommandInvoker.class.getName());
    
    private HashMap<String, Command> commands;

    /**
     * Initializes command objects
     * 
     * @param db
     *            A connection handle to the database
     * @param chatManager
     *            A reference to the ChatManager object
     * @param clientManager
     *            A reference to the ClientManager object
     * @param gameManager
     *            A reference to the GameManager object
     */
    public ClientCommandInvoker(Connection db, ChatManager chatManager, ClientManager clientManager,
            GameManager gameManager) {
        this.commands = new HashMap<>();
        this.commands.put("NewUser", new ClientNewUser(db));
        this.commands.put("Login", new ClientLogin(db, clientManager));
        this.commands.put("ChatEvent", new ClientChatEvent(chatManager, clientManager));
        this.commands.put("SendChat", new ClientSendChatMessage(chatManager, clientManager));
        this.commands.put("GetChatList", new ClientGetChatList(chatManager));
        this.commands.put("CreateChat", new ClientCreateChat(chatManager));
        this.commands.put("QueueRandom", new ClientQueueRandomGame(gameManager));
        this.commands.put("FromCThrowDice", new ClientThrowDice(gameManager));
        this.commands.put("FromCMovePiece", new ClientMovePiece(gameManager));
        this.commands.put("ChallengeStart", new ClientChallengeStart(gameManager));
        this.commands.put("ChallengePlayer", new ClientChallengePlayer(gameManager));
        this.commands.put("ChallengeResponse", new ClientChallengeResponse(gameManager));
        this.commands.put("ChallengeAbort", new ClientChallengeAbort(gameManager));
        this.commands.put("ChallengeStartGame", new ClientChallengeStartGame(gameManager));
    }

    /**
     * Executes a command from a specific client
     */
    @Override
    public void execute(ClientThread client, String[] params) {
        Command command = commands.get(params[0]);
        if (command != null) {
            command.execute(client, Arrays.copyOfRange(params, 1, params.length));
        } else {
            LOGGER.warning("Unknown client command: " + params[0]);
        }
    }
}
