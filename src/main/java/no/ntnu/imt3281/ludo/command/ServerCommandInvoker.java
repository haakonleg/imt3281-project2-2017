package no.ntnu.imt3281.ludo.command;

import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * This is the invoker class, responsible for receiving commands from the server
 * and executing them. A hashmap is used with a string and a ServerCommand
 * object. When the server receives commands this invoker parses the command
 * string and executes the correct ServerCommand object with parameters.
 * 
 * @author Hakkon
 *
 */
public class ServerCommandInvoker implements Command {
    private static final Logger LOGGER = Logger.getLogger(ServerCommandInvoker.class.getName());
    
    private HashMap<String, Command> commands;

    /**
     * This adds all commands to the HashMap
     * @param chatManager A reference to the ChatManager object, used by many commands
     * @param client Client which is executing this commands
     */
    public ServerCommandInvoker(ChatManager chatManager, Client client) {
        this.commands = new HashMap<>();
        this.commands.put("ChatMsgSent", new ServerChatMessageSent(chatManager));
        this.commands.put("ChatUsrEvent", new ServerChatUserEvent(chatManager));
        this.commands.put("StartGame", new ServerStartGame(client));
        this.commands.put("FromSDiceEvent", new ServerDiceEvent(client));
        this.commands.put("FromSMovePiece", new ServerMoveEvent(client));
        this.commands.put("FromSPlayerEvent", new ServerPlayerEvent(client));
        this.commands.put("ChallengeIssued", new ServerChallengeIssued(client));
        this.commands.put("ChallengeResponse", new ServerChallengeResponse(client));
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        Command command = commands.get(params[0]);
        if (command != null) {
            command.execute(client, Arrays.copyOfRange(params, 1, params.length));
        } else {
            LOGGER.log(Level.WARNING, "Unknown server command: " + params[0]);
        }
    }
}
