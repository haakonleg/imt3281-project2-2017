package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * Command which is run when a client in a game moves a piece
 *
 */
public class ClientMovePiece implements Command {

    private GameManager gameManager;

    /**
     * Constructor
     * @param gm Reference to game manager object
     */
    public ClientMovePiece(GameManager gm) {
        this.gameManager = gm;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        String id = params[0];
        int from = Integer.parseInt(params[1]);
        int to = Integer.parseInt(params[2]);

        this.gameManager.receiveMovePiece(client, id, from, to);

    }

}
