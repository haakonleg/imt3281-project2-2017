package no.ntnu.imt3281.ludo.command;

import javafx.application.Platform;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.gui.ChallengeController;

/**
 * Command to send a response to server on a game challenge request
 * @author Hakkon
 *
 */
public class ServerChallengeResponse implements Command {

    private Client client;

    /**
     * Constructor
     * @param c Reference to client object
     */
    public ServerChallengeResponse(Client c) {
        this.client = c;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        ChallengeController challengeController = this.client.getChallengeController();

        if (challengeController != null) {
            Platform.runLater(() -> challengeController.receiveChallengeUpdate(params[0], params[1]));
        }

    }

}
