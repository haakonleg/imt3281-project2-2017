package no.ntnu.imt3281.ludo.command;

import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * Command which is run when a challenge results in a game being started
 *
 */
public class ClientChallengeStartGame implements Command {
    private GameManager gameManager;

    /**
     * Constructor
     * @param gm Reference to game manager object
     */
    public ClientChallengeStartGame(GameManager gm) {
        this.gameManager = gm;
    }

    @Override
    public void execute(ClientThread client, String[] params) {
        this.gameManager.challengeStartGame(client);
    }
}
