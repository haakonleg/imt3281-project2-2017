package no.ntnu.imt3281.ludo.logic;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thrown if the name that was searched on doesn't exist
 *
 */
public class NoSuchPlayerException extends RuntimeException {
    private static final Logger LOGGER = Logger.getLogger(NoSuchPlayerException.class.getName());
    
    private static final long serialVersionUID = 1L;

    NoSuchPlayerException(String message) {
        LOGGER.log(Level.SEVERE, message);
    }
}
