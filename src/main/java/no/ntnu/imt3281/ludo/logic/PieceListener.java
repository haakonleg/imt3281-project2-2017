package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * Generic PieceListener
 *
 */
public interface PieceListener extends EventListener {
    
    /**
     * Function for when a piece is moved
     * @param event PieceEvent belonging to a particular piece move
     */
    public void pieceMoved(PieceEvent event);
}
