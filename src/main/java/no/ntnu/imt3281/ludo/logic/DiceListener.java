package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * Generic DiceListener
 *
 */
public interface DiceListener extends EventListener {
    
    /**
     * Function for when a dice is thrown 
     * @param event DiceEvent belonging to a particular dice throw
     */
    public void diceThrown(DiceEvent event);
}
