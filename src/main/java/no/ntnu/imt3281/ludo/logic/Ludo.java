package no.ntnu.imt3281.ludo.logic;

import java.util.ArrayList;
import java.util.Random;

/**
 * Standard Ludo class that executes all game operations according to game rules
 * 
 * @author Maciek
 *
 */
public class Ludo {

    public static final int RED = 0;
    public static final int BLUE = 1;
    public static final int YELLOW = 2;
    public static final int GREEN = 3;

    private ArrayList<String> players;
    private int activePlayer;
    private int dice;
    private boolean moveSpent;

    private Random randomGenerator;

    private int[][] playerPieces;

    private ArrayList<DiceListener> diceListeners;
    private ArrayList<PieceListener> pieceListeners;
    private ArrayList<PlayerListener> playerListeners;

    /**
     * Initializes containers, and adds one of each listeners to the listeners
     * vectors
     */
    public Ludo() {
        this.players = new ArrayList<>(4);
        this.playerPieces = new int[4][4];
        this.diceListeners = new ArrayList<>();
        this.pieceListeners = new ArrayList<>();
        this.playerListeners = new ArrayList<>();
        this.randomGenerator = new Random();

        this.addDiceListener(new LudoDiceListener());
        this.addPieceListener(new LudoPieceListener());
        this.addPlayerListener(new LudoPlayerListener());
    }

    /**
     * Real initialization point. Needs to be called with at least 2 names to start
     * a proper game. As the Ludo class is not built to let users choose their
     * color, names need to be put in param order So, if 2 players then red and
     * blue, 3 players then red, blue and yellow, with the rest as null. If less
     * players than 4, set remaining parameters to null.
     * 
     * @param red
     *            Name of the red player.
     * @param blue
     *            Name of the blue player.
     * @param yellow
     *            Name of the yellow player.
     * @param green
     *            Name of the green player.
     * @throws NotEnoughPlayersException
     *             Thrown in the event there are less than 2 players.
     */
    public Ludo(String red, String blue, String yellow, String green) throws NotEnoughPlayersException {
        // Call the other constructor
        this();

        if (red != null) {
            this.addPlayer(red);
        } 
        if (blue != null) {
            this.addPlayer(blue);
        } 
        if (yellow != null) {
            this.addPlayer(yellow);
        } 
        if (green != null) {
            this.addPlayer(green);
        }

        // Game always starts with red throwing first
        this.activePlayer = RED;
        PlayerEvent firstUser = new PlayerEvent(this, this.activePlayer, PlayerEvent.PLAYING);
        for (PlayerListener playerListener : this.playerListeners) {
            playerListener.playerStateChanged(firstUser);
        }

        // Game needs at least 2 players
        if (this.nrOfPlayers() < 2) {
            throw new NotEnoughPlayersException("a game needs minimum two players");
        }
    }

    /**
     * Converts the playerPieces array from local coordinates for each player to the
     * common coordinate system.
     * 
     * @return A playerPieces object converted from each players local coordinates,
     *         to common grid coordinates
     */
    private int[][] getUserGridToPlayGrid() {
        int[][] convertedGrid = new int[4][4];

        // For each player's piece
        for (int player = 0; player <= 3; player++) {
            for (int piece = 0; piece <= 3; piece++) {
                convertedGrid[player][piece] = this.userGridToLudoBoardGrid(player, this.playerPieces[player][piece]);
            }
        }

        return convertedGrid;
    }

    /**
     * Checks if all pieces of the current user are in the home position.
     * 
     * @return TRUE if all 4 pieces are home, FALSE if not.
     */
    public boolean allHome() {
        for (int i = 0; i < 4; i++) {
            if (this.playerPieces[this.activePlayer][i] != 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Gives the turn to the next player, firing PlayerEvents and resetting the dice
     * on all listeners
     */
    private void nextPlayer() {
        //Check if the winner has been declared, if so, next player can't be selected
        this.checkWinner();
        if ((((LudoPlayerListener) this.playerListeners.get(0)).lastEventStatus() == PlayerEvent.WON)) {
            return;
        }
        
        // Check if the last event was a LEFTGAME event, and if NOT, fire a WAITING
        // event on all listeners for current player
        if (((LudoPlayerListener) this.playerListeners.get(0)).lastEventStatus() != PlayerEvent.LEFTGAME) {
            PlayerEvent oldUser = new PlayerEvent(this, this.activePlayer, PlayerEvent.WAITING);
            for (PlayerListener playerListener : this.playerListeners) {
                playerListener.playerStateChanged(oldUser);
            }
        }

        // Move activePlayer to next player until a valid player name is reached
        do {
            this.activePlayer++;
            if (this.activePlayer >= this.players.size()) {
                this.activePlayer = 0;
            }
        } while (this.players.get(this.activePlayer).startsWith("****"));

        // Fire a PLAYING event on the next player to play
        PlayerEvent newUser = new PlayerEvent(this, this.activePlayer, PlayerEvent.PLAYING);
        for (PlayerListener playerListener : this.playerListeners) {
            playerListener.playerStateChanged(newUser);
        }

        // Reset the dice throws for the next player
        for (DiceListener diceListener : this.diceListeners) {
            if (diceListener instanceof LudoDiceListener) {
                ((LudoDiceListener) diceListener).resetThrows();
            }
        }
    }

    /**
     * Finds the players piece in playerPieces from pos
     * 
     * @param player
     *            Player ID (normally this.activePlayer)
     * @param pos
     *            Position where the selected piece should be
     * @return Location in the playerPieces with the piece of the user, -1 if none
     *         found
     */
    private int positionToPiece(int player, int pos) {
        // Locate the piece to move
        for (int i = 0; i < 4; i++) {
            if (this.playerPieces[player][i] == pos) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Checks if the attempted move is legal
     * 
     * @param player
     *            Player doing the move
     * @param from
     *            Location from which the player is doing the move
     * @param to
     *            Location to where the player is moving
     * @return TRUE if the move is legal, FALSE if the move is not legal
     */
    private boolean isLegalMove(int player, int from, int to) {
        // Locate the piece to move
        int piece = this.positionToPiece(player, from);

        // No piece on from position
        if (piece == -1) {
            return false;
        }

        // Special case, piece being moved back
        if (to == 0) {
            return true;
        }

        // To-from must be equal to the dice value (but not if moving from position 0)
        if (from != 0 && to - from != this.dice) {
            return false;
        }

        // Needs a six to move from position 0, and then one can only move to 1
        if (from == 0 && (this.dice != 6 || to != 1)) {
            return false;
        }
        
        // If the user threw three sixes in a row, and all pieces aren't in home
        // location
        if (((LudoDiceListener) this.diceListeners.get(0)).threeSixInARowPenalty()) {
            return false;
        }

        // Can't move past 59
        if (to > 59) {
            return false;
        }

        // At last, if there aren't two or more pieces on target location, return true
        // (the opposite of blocked is what is desired by caller)
        return !this.blocked(player, from, to);

    }

    /**
     * Checks if the user can move one of his pieces on the board.
     * 
     * @return TRUE of any of the four pieces can be moved, FALSE if none can be
     *         moved.
     */
    private boolean canMove() {
        // For all 4 pieces of the active player
        for (int i = 0; i < 4; i++) {
            // If the particular piece is in 0, you can only go to 1
            if (this.playerPieces[this.activePlayer][i] == 0) {
                if (isLegalMove(this.activePlayer, this.playerPieces[this.activePlayer][i], 1)) {
                    return true;
                }
            } else { 
                // If it isn't in 0, it can go to where dice is (maybe)
                if (isLegalMove(this.activePlayer, this.playerPieces[this.activePlayer][i],
                        this.playerPieces[this.activePlayer][i] + this.dice)) {
                    return true;
                }
            }

        }

        // If none could be moved
        return false;
    }

    /**
     * Checks if the user is blocked from moving to "to".
     * 
     * @param player
     *            Player trying to move
     * @param from
     *            Location from where the player is trying to move
     * @param to
     *            Location to which the player is trying to move
     * @return TRUE if there exists a block in the users path, FALSE if there
     *         doesn't
     */
    private boolean blocked(int player, int from, int to) {
        // Converted playerPieces to a common coordinate system
        int[][] convertedGrid = this.getUserGridToPlayGrid();

        // Go for each step from the "from" location, to and including "to" location.
        for (int check = from + 1; check <= to; check++) {
            int onSpot = 0;
            int target = this.userGridToLudoBoardGrid(player, check); 

            // For each player in this step
            for (int playerIterate = 0; playerIterate <= 3; playerIterate++) {
                // Skip current player
                if (playerIterate == player) {
                    continue;
                }

                
                
                // For each piece
                for (int piece = 0; piece <= 3; piece++) {
                    // Check if piece location matches target, increment onSpot if TRUE
                    onSpot += (convertedGrid[playerIterate][piece] == target) ? 1 : 0;
                }
            }

            // At the end, if there were more than one piece on the spot the player is
            // moving through, return TRUE
            if (onSpot > 1) {
                return true;
            }
        }

        // If nothing blocked the user, return FALSE
        return false;
    }

    /**
     * Checks if a player has won, and fires a WON PlayerEvent to all listeners if
     * so
     */
    private void checkWinner() {
        int winnerID = this.getWinner();

        // If the winnerID isn't the "no winner ID"
        if (winnerID != -1) {
            PlayerEvent winner = new PlayerEvent(this, winnerID, PlayerEvent.WON);
            for (PlayerListener playerListener : this.playerListeners) {
                playerListener.playerStateChanged(winner);
            }
        }
    }

    /**
     * Converts provided pos of the provided player with the common coordinate
     * 
     * @param player
     *            Player whose pos to convert
     * @param pos
     *            Local coordinate belonging to the player
     * @return The converted common coordinate
     */
    public int userGridToLudoBoardGrid(int player, int pos) {
        int retPos = 0;
        int startOffset = 15;

        // If the position is in starting location
        if (pos == 0) {
            retPos = 4 * player;
        }

        // On the common spots, excluding the full circle position
        if (pos > 0 && pos < 53) {
            retPos = pos + startOffset;
            retPos += player * 13;
            
            // If the coordinate goes above this number, position needs another calculation
            if (retPos > 67) { 
                retPos = pos - 37;
                retPos += player * 13;
            }
        }

        // The full circle position, same spot where players move out from home
        if (pos == 53) {
            retPos = 16;
            retPos += player * 13;
        }

        // On the final, player-only positions
        if (pos > 53) {
            retPos = pos + 14;
            retPos += player * 6;
        }

        return retPos;
    }

    /**
     * Player number that started in this playthrough.
     * 
     * @return Players at start
     */
    public int nrOfPlayers() {
        return this.players.size();
    }

    /**
     * Players that are still playing
     * 
     * @return Players currently playing
     */
    public int activePlayers() {
        int cnt = this.nrOfPlayers();

        // Remove all players starting with **** name (inactive players)
        for (String player : this.players) {
            if (player.startsWith("****")) {
                cnt--;
            }
        }
        return cnt;
    }

    /**
     * Returns the player name from the player ID
     * 
     * @param player
     *            Player ID
     * @return Name of the player
     */
    public String getPlayerName(int player) {
        // Player has to be between 0 and the amount of players that started the game.
        if (player >= 0 && player >= this.players.size()) {
            return null;
        }

        String playerName = this.players.get(player);

        // If the player has left, remove the 4 stars from the name that were appended
        // when he left
        if (playerName.startsWith("****")) {
            return "Inactive: " + playerName.substring(4, playerName.length());
        } else {
            return playerName;
        }
    }

    /**
     * Adds a player to the game
     * 
     * @param player
     *            Name of the player
     */
    public void addPlayer(String player) {
        // Name can't start with 4 stars, used to mark inactive players
        if (player.startsWith("****")) {
            throw new IllegalPlayerNameException("name cannot start with 4 stars");
        }

        // If there is room in the game, add, otherwise throw another exception
        if (this.players.size() < 4) {
            this.players.add(player);
        } else {
            throw new NoRoomForMorePlayersException("only 4 players can be in one game");
        }
    }

    /**
     * Changes a player status to inactive, removing such player from the game
     * 
     * @param player
     *            Player name to be removed
     * @throws NoSuchPlayerException
     *             Thrown if the name isn't used by anyone in the game
     */
    public void removePlayer(String player) {
        int index = this.players.indexOf(player);

        // If the player exists in the players String Vector
        if (index != -1) {
            // Append the inactive **** marker to the name
            this.players.set(index, "****" + this.players.get(index));

            // Notify listeners that player left
            PlayerEvent userLeft = new PlayerEvent(this, index, PlayerEvent.LEFTGAME);
            for (PlayerListener playerListener : this.playerListeners) {
                playerListener.playerStateChanged(userLeft);
            }
            
            //If all players quit
            if (this.activePlayers() == 0) {
                return;
            }
            
            // If the leaving player was also the active player, give the turn to the next
            // player
            if (this.activePlayer == index) {
                this.nextPlayer();
            }
        } else { 
            // Player wasn't found
            throw new NoSuchPlayerException(player + " does not exist in this game");
        }
    }

    /**
     * Gives access to the position for the given player's piece
     * 
     * @param player
     *            Player to whom the pieces belong to
     * @param piece
     *            The particular piece to get location of
     * @return Players local coordinate for the piece
     */
    public int getPosition(int player, int piece) {
        return this.playerPieces[player][piece];
    }

    /**
     * Gives access to the currently active player
     * 
     * @return The ID of the active player
     */
    public int activePlayer() {
        return this.activePlayer;
    }

    /**
     * Generates a random number between 1 and 6, calls throwDice(int) and returns
     * generated result
     * 
     * @return Generated number
     */
    public int throwDice() {
        // Safety check, if the users move isn't spent, don't let the user throw another
        // one
        if (this.moveSpent) {
            return this.throwDice(this.randomGenerator.nextInt(6) + 1);
        } else {
            return this.dice;
        }

    }

    /**
     * Fires DiceEvents to all listeners, sets dice, and if the user can't move, and
     * it was his last dice roll, changes player
     * 
     * @param serverDice
     *            Value between 1-6 that was randomly generated in throwDice (or
     *            preset for testing purposes)
     * @return Same as param serverDice
     */
    public int throwDice(int serverDice) {
        DiceEvent thrown = new DiceEvent(this, this.activePlayer, serverDice);
        for (DiceListener diceListener : this.diceListeners) {
            diceListener.diceThrown(thrown);
        }
        this.dice = serverDice;

        if (this.allHome() && this.dice != 6) {
            // Reset the dice throws for the next player
            this.moveSpent = true;
        }

        if (((LudoDiceListener) this.diceListeners.get(0)).usersLastDiceRoll() && !this.canMove()) {
            // Reset the dice throws for the next player
            this.moveSpent = true;
            this.nextPlayer();
        }
        return serverDice;
    }

    /**
     * Moves the player's piece from the "from" location to the "to" location Can be
     * used to move any piece back to home
     * 
     * @param player
     *            Player whose pieces to move
     * @param from
     *            Location from which to move
     * @param to
     *            Location to which to move
     * @return TRUE if the piece was moved, FALSE if not
     */
    public boolean movePiece(int player, int from, int to) {
        // If the move requested is not legal, return false
        if (!this.isLegalMove(player, from, to)) {
            return false;
        }

        // Checks passed, move the piece
        int piece = this.positionToPiece(player, from);
        this.playerPieces[player][piece] = to;

        // Fire a PieceEvent that a piece has been moved to all listeners
        PieceEvent thrown = new PieceEvent(this, player, piece, from, to);
        for (PieceListener pieceListener : this.pieceListeners) {
            pieceListener.pieceMoved(thrown);
        }

        // If the piece was moved back to home, quit executing here
        if (to == 0) {
            return true;
        }

        // Classify the move as spent
        this.moveSpent = true;

        // Initialize a common coordinate grid, and the location to which the player is
        // moving in common coord
        int[][] convertedGrid = this.getUserGridToPlayGrid();
        int target = this.userGridToLudoBoardGrid(player, to);

        // For each player
        for (int playerIterate = 0; playerIterate <= 3; playerIterate++) {
            // Skip current player
            if (playerIterate == player) {
                continue;
            }

            // For all pieces of playerIterate
            for (int pieces = 0; pieces <= 3; pieces++) {
                // If there is a piece where the player wants to move, move said piece back to
                // its home
                if (convertedGrid[playerIterate][pieces] == target) {
                    movePiece(playerIterate, this.playerPieces[playerIterate][pieces], 0);
                }
            }
        }

        // If the player can't roll dice, or was moving from 0, change player
        if (((LudoDiceListener) this.diceListeners.get(0)).usersLastDiceRoll(from)) {
            this.nextPlayer();
        }

        // All operations complete
        return true;
    }

    /**
     * Returns the status of the game
     * 
     * @return Finished if the game is over, Started if the game is underway,
     *         Created if nrOfPlayers can be called, Initiated otherwise
     */
    public String getStatus() {
        if (this.getWinner() != -1) {
            return "Finished";
        } else if (this.dice != 0) {
            return "Started";
        } else if (this.nrOfPlayers() == 0) {
            return "Created";
        } else {
            return "Initiated";
        }
    }

    /**
     * Checks if a player has all his pieces at exit
     * 
     * @return Player ID if the player has all pieces at exit, -1 if nobody does
     */
    public int getWinner() {
        for (int player = 0; player <= 3; player++) {
            int piecesAtExit = 0;

            // For each piece
            for (int piece = 0; piece <= 3; piece++) {
                // If the piece is at local coord 59 (exit)
                if (this.playerPieces[player][piece] == 59) {
                    piecesAtExit++;
                }
            }

            // If all pieces at exit, return player
            if (piecesAtExit == 4) {
                return player;
            }
        }

        // If no winner was found
        return -1;
    }

    /**
     * Adds a DiceListener to the diceListeners vector
     * 
     * @param listener
     *            A DiceListener to add
     */
    public void addDiceListener(DiceListener listener) {
        this.diceListeners.add(listener);
    }

    /**
     * Adds a PieceListener to the pieceListeners vector
     * 
     * @param listener
     *            A PieceListener to add
     */
    public void addPieceListener(PieceListener listener) {
        this.pieceListeners.add(listener);
    }

    /**
     * Adds a PlayerListener to the playerListeners vector
     * 
     * @param listener
     *            A PlayerListener to add
     */
    public void addPlayerListener(PlayerListener listener) {
        this.playerListeners.add(listener);
    }
}
