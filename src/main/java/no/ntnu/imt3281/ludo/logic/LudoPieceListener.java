package no.ntnu.imt3281.ludo.logic;

import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * Standard PieceListener used in the Ludo class
 *
 */
public class LudoPieceListener implements PieceListener {

    // Last event received
    private PieceEvent lastEvent;
    private GameManager gameManager;
    private String gameID;

    /**
     * Sets the lastEvent on the event, not used for anything at the moment
     * 
     * @param event
     *            A PieceEvent
     */
    @Override
    public void pieceMoved(PieceEvent event) {
        this.lastEvent = event;

        if (this.gameManager != null) {
            this.gameManager.sendMoveToUsers(this.lastEvent, this.gameID);
        }
    }

    /**
     * Link this listener to a GameManager, and set the game id to which the game belongs to
     * @param gm GameManager
     * @param id Unique ID of the game
     */
    public void linkGameManager(GameManager gm, String id) {
        this.gameManager = gm;
        this.gameID = id;
    }
}
