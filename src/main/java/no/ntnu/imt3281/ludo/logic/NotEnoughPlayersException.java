package no.ntnu.imt3281.ludo.logic;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thrown if the Ludo game tries to start with less than 2 players
 *
 */
public class NotEnoughPlayersException extends RuntimeException {
    private static final Logger LOGGER = Logger.getLogger(NotEnoughPlayersException.class.getName());
    
    private static final long serialVersionUID = 1L;

    NotEnoughPlayersException(String message) {
        LOGGER.log(Level.SEVERE, message);
    }
}
