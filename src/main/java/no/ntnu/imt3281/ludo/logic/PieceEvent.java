package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.Objects;

/**
 * A particular PieceEvent fired by the Ludo class.
 *
 */
public class PieceEvent extends EventObject {
    private static final long serialVersionUID = 1L;

    private int player;
    private int piece;
    private int from;
    private int to;

    /**
     * Initializes the EventObject which PieceEvent extends
     * @param object EventObject prototype
     */
    public PieceEvent(Object object) {
        super(object);
    }

    /**
     * Initializes the object
     * 
     * @param object Prototype EventObject
     * @param player
     *            Player ID
     * @param piece
     *            Piece to be moved
     * @param from
     *            Location to move from
     * @param to
     *            Location to move to
     */
    public PieceEvent(Object object, int player, int piece, int from, int to) {
        super(object);
        this.player = player;
        this.piece = piece;
        this.from = from;
        this.to = to;
    }

    /**
     * Returns the player ID
     * 
     * @return player
     */
    public int getPlayer() {
        return this.player;
    }

    /**
     * Sets the player
     * 
     * @param player
     *            Player ID
     */
    public void setPlayer(int player) {
        this.player = player;
    }

    /**
     * Returns the piece to be moved
     * 
     * @return piece
     */
    public int getPiece() {
        return this.piece;
    }

    /**
     * Sets the piece to be moved
     * 
     * @param piece
     */
    public void setPiece(int piece) {
        this.piece = piece;
    }

    /**
     * Returns the location to move from
     * 
     * @return from
     */
    public int getFrom() {
        return this.from;
    }

    /**
     * Sets the location to move from
     * 
     * @param from
     */
    public void setFrom(int from) {
        this.from = from;
    }

    /**
     * Returns the location to move to
     * 
     * @return to
     */
    public int getTo() {
        return this.to;
    }

    /**
     * Sets the location to move to
     * 
     * @param to
     */
    public void setTo(int to) {
        this.to = to;
    }

    /**
     * Compares this object with the other object in param
     * 
     * @param other
     *            Object to compare against
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        } else if (other == this) {
            return true;
        } else if (!(other instanceof PieceEvent)) {
            return false;
        }
        
        PieceEvent otherPieceEvent = (PieceEvent) other;
        return otherPieceEvent.player == this.player && otherPieceEvent.piece == this.piece
                && otherPieceEvent.from == this.from && otherPieceEvent.to == this.to;
    }

    /**
     * Returns an int hash value generated on player, piece, from and to
     * 
     * @return Hash as int
     */
    @Override
    public int hashCode() {
        return Objects.hash(player, piece, from, to);
    }
}
