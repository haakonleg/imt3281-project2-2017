package no.ntnu.imt3281.ludo.logic;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thrown if someone tries to use an illegal name in the Ludo game (for example
 * **** at start)
 *
 */
public class IllegalPlayerNameException extends RuntimeException {
    private static final Logger LOGGER = Logger.getLogger(IllegalPlayerNameException.class.getName());
    
    private static final long serialVersionUID = 1L;

    IllegalPlayerNameException(String message) {
        LOGGER.log(Level.SEVERE, message);
    }
}
