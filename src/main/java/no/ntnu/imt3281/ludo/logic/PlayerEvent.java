package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.Objects;

/**
 * A particular PlayerEvent fired by the Ludo class.
 *
 */
public class PlayerEvent extends EventObject {
    private static final long serialVersionUID = 1L;

    public static final int PLAYING = 0;
    public static final int WAITING = 1;
    public static final int LEFTGAME = 2;
    public static final int WON = 3;

    private int activePlayer;
    private int state;

    /**
     * Initializes the EventObject which PlayerEvent extends
     * @param object EventObject prototype
     */
    PlayerEvent(Object object) {
        super(object);
    }

    /**
     * Initializes the object
     * 
     * @param object Prototype EventObject
     * @param activePlayer
     *            Player to which the event belongs
     * @param state
     *            Which state the player is changed to
     */
    PlayerEvent(Object object, int activePlayer, int state) {
        super(object);
        this.activePlayer = activePlayer;
        this.state = state;
    }

    /**
     * Returns the player the event belongs to
     * 
     * @return activePlayer
     */
    public int getActivePlayer() {
        return this.activePlayer;
    }

    /**
     * Sets the player to which the event will belong
     * 
     * @param player
     */
    public void setActivePlayer(int player) {
        this.activePlayer = player;
    }

    /**
     * Returns the state in which the player is
     * 
     * @return state
     */
    public int getState() {
        return this.state;
    }

    /**
     * Sets the state of the player
     * 
     * @param state
     */
    public void setState(int state) {
        this.state = state;
    }

    /**
     * Compares this object with the other object in param
     * 
     * @param other
     *            Object to compare against
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        } else if (other == this) {
            return true;
        } else if (!(other instanceof PlayerEvent)) {
            return false;
        }

        PlayerEvent otherPlayerEvent = (PlayerEvent) other;
        return otherPlayerEvent.activePlayer == this.activePlayer && otherPlayerEvent.state == this.state;
    }

    /**
     * Returns an int hash value generated on activePlayer and state
     * 
     * @return Hash as int
     */
    @Override
    public int hashCode() {
        return Objects.hash(activePlayer, state);
    }
}
