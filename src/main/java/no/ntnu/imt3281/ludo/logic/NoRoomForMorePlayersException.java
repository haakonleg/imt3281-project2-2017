package no.ntnu.imt3281.ludo.logic;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thrown if there isn't any more room in the Ludo game, and someone still tries
 * to add more players
 *
 */
public class NoRoomForMorePlayersException extends RuntimeException {
    private static final Logger LOGGER = Logger.getLogger(NoRoomForMorePlayersException.class.getName());
    
    private static final long serialVersionUID = 1L;

    NoRoomForMorePlayersException(String message) {
        LOGGER.log(Level.SEVERE, message);
    }
}
