package no.ntnu.imt3281.ludo.logic;

import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * Standard PlayerListener used in the Ludo class
 *
 */
public class LudoPlayerListener implements PlayerListener {

    // Last event to be received
    PlayerEvent lastEvent;
    private GameManager gameManager;
    private String gameID;

    /**
     * Sets the lastEvent
     * 
     * @param event
     *            A PlayerEvent
     */
    @Override
    public void playerStateChanged(PlayerEvent event) {
        this.lastEvent = event;

        if (this.gameManager != null) {
            this.gameManager.sendPlayerToUsers(this.lastEvent, this.gameID);
        }
    }

    /**
     * Returns the state on the last PlayerEvent received.
     * 
     * @return A PlayerEvent constant, PLAYING (0) WAITING (1) LEFTGAME (2) WON (3)
     */
    public int lastEventStatus() {
        return lastEvent.getState();
    }

    /**
     * Returns the player who the event belongs to.
     * 
     * @return Player ID
     */
    public int lastEventPlayer() {
        return lastEvent.getActivePlayer();
    }

    /**
     * Link this listener to a GameManager, and set the game id to which the game belongs to
     * @param gm GameManager
     * @param id Unique ID of the game
     */
    public void linkGameManager(GameManager gm, String id) {
        this.gameManager = gm;
        this.gameID = id;
    }
}
