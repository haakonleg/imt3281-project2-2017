package no.ntnu.imt3281.ludo.logic;

import no.ntnu.imt3281.ludo.server.GameManager;

/**
 * Standard DiceListener used in the Ludo class
 *
 */
public class LudoDiceListener implements DiceListener {

    // Dice rolls done since last reset
    private int throwsDone = 0;
    // Latest DiceEvent event
    private DiceEvent lastEvent;
    // Player has got three 6s in a row
    private boolean sixInARow = false;
    private GameManager gameManager;
    private String gameID;

    /**
     * Sets lastEvent, increments throwsDone and checks if the user got six in a row
     * 
     * @param event
     *            A DiceEvent
     */
    @Override
    public void diceThrown(DiceEvent event) {
        this.lastEvent = event;
        this.throwsDone++;

        if (this.throwsDone == 3 && lastEvent.getDice() == 6) {
            this.sixInARow = true;
        }

        if (gameManager != null) {
            this.gameManager.sendDiceToUsers(this.lastEvent, this.gameID);
        }
    }

    /**
     * Checks if the user is on his last dice roll
     * @return TRUE if the user can still throw the dice, FALSE if not
     */
    public boolean usersLastDiceRoll() {
        return this.usersLastDiceRoll(-1);
    }
    
    /**
     * Checks if the user is on his last dice roll
     * @param from Location user moved from, -1 if not applicable
     * @return TRUE if the user can still throw the dice, FALSE if not
     */
    public boolean usersLastDiceRoll(int from) {
        // If the player has all his pieces at home position, and hasn't thrown the dice 3 times
        if (((Ludo) this.lastEvent.getSource()).allHome() && this.throwsDone < 3) {
            return false;
        }
        
        if (from == 0) {
            return true;
        }
        
        // If the player has rolled a 6 and it isn't his 3rd try FALSE, if the user can
        // still throw TRUE
        return !(this.throwsDone < 3 && this.lastEvent.getDice() == 6);
    }

    /**
     * Resets the sixInARow and throwsDone to their original values
     */
    public void resetThrows() {
        this.sixInARow = false;
        this.throwsDone = 0;
    }

    /**
     * Returns the sixInARow boolean, 
     *  if all pieces are home, permits move even if 3 are thrown
     * 
     * @return sixInARow, or false if user has all pieces at home
     */
    public boolean threeSixInARowPenalty() {
        if (((Ludo) this.lastEvent.getSource()).allHome() && this.sixInARow) {
            return false;
        }
        return this.sixInARow;
    }

    /**
     * Return the last dice event
     * @return lastEvent
     */
    public DiceEvent retLastEvent() {
        return this.lastEvent;
    }

    /**
     * Link this listener to a GameManager, and set the game id to which the game belongs to
     * @param gm GameManager
     * @param id Unique ID of the game
     */
    public void linkGameManager(GameManager gm, String id) {
        this.gameManager = gm;
        this.gameID = id;
    }
}
