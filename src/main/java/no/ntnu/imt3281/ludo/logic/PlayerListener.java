package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * Generic PlayerListener
 *
 */
public interface PlayerListener extends EventListener {
    
    /**
     * Function for when a player state is changed
     * @param event PlayerEvent belonging to a particular player state change
     */
    public void playerStateChanged(PlayerEvent event);
}
