package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.Objects;

/**
 * A particular DiceEvent fired by the Ludo class.
 *
 */
public class DiceEvent extends EventObject {
    private static final long serialVersionUID = 1L;

    private int player;
    private int dice;

    /**
     * Initializes the EventObject which DiceEvent extends
     * @param object EventObject prototype
     */
    public DiceEvent(Object object) {
        super(object);
    }

    /**
     * Initializes the player and dice in the event
     * 
     * @param object Prototype EventObject
     * @param player
     *            Player ID
     * @param val
     *            Dice value (1-6)
     */
    public DiceEvent(Object object, int player, int val) {
        super(object);
        this.player = player;
        this.dice = val;
    }

    /**
     * Returns the player
     * 
     * @return player
     */
    public int getPlayer() {
        return player;
    }

    /**
     * Sets the player
     * 
     * @param player
     *            Player ID
     */
    public void setPlayer(int player) {
        this.player = player;
    }

    /**
     * Returns the dice
     * 
     * @return dice
     */
    public int getDice() {
        return this.dice;
    }

    /**
     * Sets the dice
     * 
     * @param dice
     *            Dice value (1-6)
     */
    public void setDice(int dice) {
        this.dice = dice;
    }

    /**
     * Compares this object with the other object in param
     * 
     * @param other
     *            Object to compare against
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        } else if (other == this) {
            return true;
        } else if (!(other instanceof DiceEvent)) {
            return false;
        }

        DiceEvent otherDiceEvent = (DiceEvent) other;
        return otherDiceEvent.dice == this.dice && otherDiceEvent.player == this.player;
    }

    /**
     * Returns an int hash value generated on player and dice
     * 
     * @return Hash as int
     */
    @Override
    public int hashCode() {
        return Objects.hash(player, dice);
    }
}
