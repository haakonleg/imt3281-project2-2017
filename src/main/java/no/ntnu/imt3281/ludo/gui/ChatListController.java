package no.ntnu.imt3281.ludo.gui;

import java.util.ArrayList;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * Controller for ChatList.fxmml
 * 
 * @author Hakkon
 *
 */
public class ChatListController {

    @FXML
    private Button createChatBtn;

    @FXML
    private TableView<ChatListTable> chatTable;

    @FXML
    private TableColumn<ChatListTable, String> chatNameCol;

    @FXML
    private TableColumn<ChatListTable, Integer> usrCountCol;

    @FXML
    private TableColumn<ChatListTable, String> dateCreatedCol;

    @FXML
    private ContextMenu contextMenu;

    private Client client;
    private TextInputDialog inputDialog;
    private LudoController ludoController;
    private ObservableList<ChatListTable> chatList;
    private Gson gson;

    /**
     * Constructor for controller
     * 
     * @param client
     *            A reference to the ClientThread object
     * @param ludoController
     *            A reference to the ludoController object
     * @param chatList
     *            A list of ChatListTable objects, which is the object model for a
     *            row in the ChatList
     */
    public ChatListController(Client client, LudoController ludoController) {
        this.gson = new Gson();
        this.client = client;
        this.ludoController = ludoController;
        this.chatList = this.getChatRooms();

        this.inputDialog = new TextInputDialog();
        this.inputDialog.setTitle("New chatroom");
        this.inputDialog.setHeaderText(null);
        this.inputDialog.setContentText("Chatroom name:");
    }

    /**
     * Is called by JavaFX. Initializes the chat list by setting the values and
     * adding a context menu to the list.
     */
    public void initialize() {
        chatNameCol.setCellValueFactory(data -> data.getValue().getChatName());
        usrCountCol.setCellValueFactory(data -> data.getValue().getUserCount().asObject());
        dateCreatedCol.setCellValueFactory(data -> data.getValue().getCreated());

        chatTable.setItems(chatList);

        chatTable.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            if (e.getButton() == MouseButton.SECONDARY) {
                this.contextMenu.show(this.chatTable, e.getScreenX(), e.getScreenY());
            }
        });
    }
    
    private ObservableList<ChatListTable> getChatRooms() {
        // Retrieve chat rooms from server
        String response = client.getClient().sendCommandAndWait("GetChatList");
        
        ArrayList<ChatListTable> chats = this.gson.fromJson(response, new TypeToken<ArrayList<ChatListTable>>() {}.getType());
        
        return FXCollections.observableArrayList(chats);
    }

    @FXML
    void createChat(ActionEvent event) {
        Optional<String> chatName = inputDialog.showAndWait();

        chatName.ifPresent(name -> {
            String chatID = client.getClient().sendCommandAndWait("CreateChat&" + name);
            if (!"INVALID_CHAT_NAME".equals(chatID) && !"TIMEOUT".equals(chatID)) {
                this.ludoController.joinChat(chatID);
                ChatListTable newRow = new ChatListTable(chatID, name, 1, System.currentTimeMillis() / 1000L);
                this.chatList.add(newRow);
            }
        });

        this.inputDialog.getEditor().clear();
    }

    @FXML
    void cmJoinChat(ActionEvent event) {
        ChatListTable selected = this.chatTable.getSelectionModel().selectedItemProperty().get();
        this.ludoController.joinChat(selected.getChatID());
    }

    @FXML
    void refresh(ActionEvent event) {
        this.chatList.setAll(this.getChatRooms());
    }
}
