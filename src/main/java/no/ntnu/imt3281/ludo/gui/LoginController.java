package no.ntnu.imt3281.ludo.gui;

import java.io.FileInputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.RememberMeToken;
import no.ntnu.imt3281.ludo.common.User;

/**
 * Controller for LoginDialog.fxml Class which is responsible for logging in a
 * user
 * 
 * @author Hakkon
 *
 */
public class LoginController {
    private static final Logger LOGGER = Logger.getLogger(LoginController.class.getName());
    
    private Client client;
    private ResourceBundle resource;
    
    private static final String TOKEN_FILE = "token.bin";
    private static final String ERROR_DIALOG = "error";

    @FXML
    private TextField usernameField;

    @FXML
    private Button loginBtn;

    @FXML
    private PasswordField passwordField;

    @FXML
    private CheckBox rememberMe;

    @FXML
    private Button newUserBtn;

    private Gson gson;
    private RememberMeToken loginToken;

    /**
     * Controller constructor. Gets a reference to the ClientThread object
     * 
     * @param client ClientThread object
     * @param res ResourceBundle with language strings
     */
    public LoginController(Client client, ResourceBundle res) {
        this.client = client;
        this.resource = res;
        this.gson = new Gson();
    }

    /**
     * Is called by JavaFX. This checks whether the client is connected to a server
     * and tries to read the "remember me" token. If it exists, the username is put
     * into field and checkbox is checked.
     */
    public void initialize() {
        if (!this.client.isConnected()) {
            this.showDialog("Connection failed", "Could not connect to server " + Client.HOSTNAME + " on port "
                    + Client.PORT + ". Make sure the server is running.", AlertType.ERROR);
            this.client.close();
            return;
        }

        // Set rememberme button as selected if login token exists
        this.loginToken = readToken();
        if (this.loginToken != null && !this.loginToken.hasExpired()) {
            this.usernameField.setText(loginToken.getUsername());
            this.usernameField.setEditable(false);
            this.passwordField.setText("AAAAAAAAAAAAAAAAAAAA");
            this.passwordField.setEditable(false);
            this.rememberMe.setSelected(true);
        }
    }

    private void showDialog(String title, String text, AlertType type) {
        Alert dialog = new Alert(type);
        dialog.setTitle(title);
        dialog.setHeaderText(null);
        dialog.setContentText(text);
        dialog.showAndWait();
        dialog = null;
    }

    /**
     * Event which is fired when the "new user" button is pressed. Contacts the
     * server to create the new user. Error dialogs are shown if errors occured.
     * 
     * @param event
     */
    @FXML
    void newUser(ActionEvent event) {
        String username = this.usernameField.getText();
        String pwd = this.passwordField.getText();

        if (username.length() == 0 || pwd.length() == 0) {
            return;
        }

        String response = client.getClient()
                .sendCommandAndWait("NewUser&" + this.usernameField.getText() + "&" + this.passwordField.getText());
        if ("OK".equals(response)) {
            this.showDialog(this.resource.getString("login.usercreated.title"), this.resource.getString("login.usercreated.text"), AlertType.INFORMATION);
        } else if ("INVALID_USERNAME".equals(response) || "INVALID_PASSWORD".equals(response)) {
            this.showDialog(this.resource.getString(ERROR_DIALOG),
                    this.resource.getString("login.error.invalidusername"),
                    AlertType.ERROR);
            this.usernameField.clear();
            this.passwordField.clear();
        } else if ("DATABASE_ERROR".equals(response)) {
            this.showDialog(this.resource.getString(ERROR_DIALOG), this.resource.getString("login.error.databasealreadyexists"), AlertType.ERROR);
            this.usernameField.clear();
            this.passwordField.clear();
        }
    }

    private void saveToken(String response) {
        RememberMeToken token = this.gson.fromJson(response, RememberMeToken.class);

        try (FileOutputStream out = new FileOutputStream(TOKEN_FILE);
                ObjectOutputStream objOut = new ObjectOutputStream(out)) {

            objOut.writeObject(token);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Could not write login token", e);
        }
    }

    private RememberMeToken readToken() {
        try (FileInputStream in = new FileInputStream(TOKEN_FILE);
                ObjectInputStream objIn = new ObjectInputStream(in)) {

            return (RememberMeToken) objIn.readObject();
        } catch (IOException | ClassNotFoundException e) {
            LOGGER.log(Level.INFO, "No login token could be read", e);
            return null;
        }
    }

    private void deleteToken() {
        try {
            Files.delete(Paths.get(TOKEN_FILE));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Could not delete login token", e);
        }
    }

    private void showMainWindow(Stage stage) {
        // Show main window
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Ludo.fxml"));
            loader.setResources(this.resource);
            LudoController controller = new LudoController(this.client, this.resource);
            loader.setController(controller);

            AnchorPane root = loader.load();
            Scene scene = new Scene(root);
            stage.setTitle(this.resource.getString("ludo.title"));
            stage.setScene(scene);

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error initializing main window", e);
        }
    }

    /**
     * Event which is fired when the "login user" button is pressed. Contacts the
     * server to login the user. Error dialogs are shown if errors occured.
     * 
     * @param event
     */
    @FXML
    void login(ActionEvent event) {
        String response = null;
        String username = this.usernameField.getText();
        String pwd = this.passwordField.getText();

        if (username.length() == 0 || pwd.length() == 0) {
            return;
        }

        // Login with token or password
        if (this.loginToken != null && this.rememberMe.isSelected()) {
            response = client.getClient().sendCommandAndWait("Login&" + username + "&" + this.loginToken.getToken()
                    + "&" + this.rememberMe.isSelected());
        } else {
            response = client.getClient().sendCommandAndWait(
                    "Login&" + username + "&" + pwd + "&" + this.rememberMe.isSelected());
        }

        if ("ALREADY_LOGGED_IN".equals(response)) {
            this.showDialog(this.resource.getString(ERROR_DIALOG), this.resource.getString("login.error.alreadyloggedin"), AlertType.ERROR);
        } else if ("USER_NOT_FOUND".equals(response) || "INVALID_PASSWORD".equals(response)) {
            this.showDialog(this.resource.getString(ERROR_DIALOG), this.resource.getString("login.error.usernotfound"), AlertType.ERROR);
            this.usernameField.clear();
            this.passwordField.clear();
        } else if ("INVALID_TOKEN".equals(response)) {
            this.showDialog(this.resource.getString(ERROR_DIALOG), this.resource.getString("login.error.invalidtoken"), AlertType.ERROR);
            this.usernameField.clear();
            this.passwordField.clear();
            this.rememberMe.setSelected(false);
            this.deleteToken();
        } else {
            // Set the user object (user id is not used)
            User user = new User(0, username);
            client.getClient().setUser(user);

            // Save login token
            if (this.rememberMe.isSelected()) {
                this.saveToken(response);
            }

            // Show main window
            Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
            this.showMainWindow(stage);

        }
    }

    /**
     * Event listener for the "remember me" checkbox. Is needed so that when it is
     * deselected the "remember me" token is deleted.
     * 
     * @param event
     */
    @FXML
    void rememberMeListener(ActionEvent event) {
        if (!this.rememberMe.isSelected() && this.loginToken != null) {
            this.deleteToken();
            this.usernameField.clear();
            this.usernameField.setEditable(true);
            this.passwordField.clear();
            this.passwordField.setEditable(true);
        }
    }

}
