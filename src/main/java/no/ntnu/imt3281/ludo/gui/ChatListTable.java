package no.ntnu.imt3281.ludo.gui;

import java.text.SimpleDateFormat;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * This is the data model for a row in the TableView used in ChatList.fxml for
 * displaying chat rooms
 * 
 * @author Hakkon
 *
 */
public class ChatListTable {
    private String chatID;

    private final SimpleStringProperty chatName;
    private final SimpleIntegerProperty nrOfUsers;
    private final SimpleStringProperty created;

    /**
     * Constructor
     * 
     * @param chatID
     *            The chatID of this chat
     * @param chatName
     *            The chat name of this chat
     * @param nrOfUsers
     *            The number of users in this chat
     * @param timestamp
     *            The timestamp in Unix time (milliseconds) when the chat was
     *            created
     */
    public ChatListTable(String chatID, String chatName, int nrOfUsers, long timestamp) {
        this.chatID = chatID;

        this.chatName = new SimpleStringProperty(chatName);
        this.nrOfUsers = new SimpleIntegerProperty(nrOfUsers);

        String time = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(timestamp);
        this.created = new SimpleStringProperty(time);
    }

    /**
     * Returns the chat ID of this chat room
     * 
     * @return chat ID
     */
    public String getChatID() {
        return this.chatID;
    }

    /**
     * Returns the name of this chat room
     * 
     * @return chat name
     */
    public SimpleStringProperty getChatName() {
        return this.chatName;
    }

    /**
     * Returns the number of users in this chat
     * 
     * @return number of users
     */
    public SimpleIntegerProperty getUserCount() {
        return this.nrOfUsers;
    }

    /**
     * Returns a formatted time string of when this chat was created
     * 
     * @return formatted time string
     */
    public SimpleStringProperty getCreated() {
        return this.created;
    }
}
