package no.ntnu.imt3281.ludo.gui;

import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.Chat;
import no.ntnu.imt3281.ludo.common.ChatUserEventListener;

/**
 * Controller for Chat.fxml
 * 
 * @author Hakkon
 *
 */
public class ChatController {
    private Client client;
    private Chat chat;
    private StringBuilder chatText;
    private boolean isGameChat;
    private ResourceBundle resource;

    @FXML
    private TextArea chatArea;

    @FXML
    private TextField msgField;

    @FXML
    private Label userCount;

    @FXML
    private ListView<String> userList;

    @FXML
    private Button sendBtn;

    /**
     * Constructor for controller, gets a reference to the ClientThread and Chat
     * room object
     * 
     * @param client
     *            ClientThread object
     * @param chat
     *            Chat room object
     * @param isGameChat
     *            Whether or not this chat room is an in-game chat room
     * @param res
     *            ResourceBundle with the language strings
     */
    public ChatController(Client client, Chat chat, boolean isGameChat, ResourceBundle res) {
        this.client = client;
        this.chat = chat;
        this.chatText = new StringBuilder();
        this.isGameChat = isGameChat;
        this.resource = res;
    }

    /**
     * Initializes the chat room. Adds chat messages from Chat object to GUI and
     * usernames.
     */
    public void initialize() {
        // Add chat messages to GUI
        for (int i = 0; i < this.chat.nrOfMessages(); i++) {
            addChatMessage(this.chat.getMessage(i).getUsername(), this.chat.getMessage(i).getMessage(),
                    this.chat.getMessage(i).getTimestamp());
        }

        // Event listener that adds chat message to text area
        this.chat.setMessageListener(e -> this.addChatMessage(e.getUsername(), e.getMessage(), e.getTimestamp()));

        // If not in-game chat
        if (!isGameChat) {

            // Add users
            for (int i = 0; i < this.chat.nrOfUsers(); i++) {
                this.addUser(this.chat.getUser(i));
            }

            // Event listener that listens for user joined and user left events
            this.chat.setUserListener(new ChatUserEventListener() {
                @Override
                public void userJoined(String username) {
                    // Run on UI thread
                    Platform.runLater(() -> ChatController.this.addUser(username));
                }

                @Override
                public void userLeft(String username) {
                    // Run on UI thread
                    Platform.runLater(() -> ChatController.this.removeUser(username));
                }
            });
        }
    }

    /**
     * Adds a chat message to the GUI
     * 
     * @param username
     *            The username to add
     * @param message
     *            The chat message contents to add
     * @param timestamp
     *            The Unix timestamp, is formatted in function
     */
    private void addChatMessage(String username, String message, long timestamp) {
        String time = new SimpleDateFormat("HH:mm:ss").format(timestamp);

        this.chatText.append(time + "  <" + username + ">  ").append(message + "\n");
        this.chatArea.setText(this.chatText.toString());
    }

    /**
     * Adds a username to "Connected users" list
     * 
     * @param user
     *            The username to add
     */
    private void addUser(String user) {
        this.userList.getItems().add(user);
        this.updateUserCount();
    }

    /**
     * Removes a user from the "Connected users" list
     * 
     * @param user
     *            The username to remove
     */
    private void removeUser(String user) {
        this.userList.getItems().remove(user);
        this.updateUserCount();
    }

    /**
     * Updates "Connected users" count
     */
    private void updateUserCount() {
        this.userCount.setText(this.resource.getString("chat.usercount") +" (" + this.chat.nrOfUsers() + ")");
    }

    /**
     * Event which is fired when a message is sent
     * 
     * @param event
     */
    @FXML
    void sendMessage(ActionEvent event) {
        String chatMsg = this.msgField.getText().replace("&", "");
        String response = client.getClient().sendCommandAndWait("SendChat&" + this.chat.getID() + "&" + chatMsg);
        if (!"OK".equals(response)) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle(this.resource.getString("error"));
            alert.setHeaderText(null);
            alert.setContentText(this.resource.getString("chat.error.send.text"));
            alert.showAndWait();
        }

        this.msgField.clear();
    }

}
