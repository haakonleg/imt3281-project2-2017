package no.ntnu.imt3281.ludo.gui;

import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * Controller responsible for the challenge window/tab
 * @author Maciek
 *
 */
public class ChallengeController {

    private Client client;
    private ResourceBundle resource;
    
    private static final String TIMEOUT_RESPONSE = "TIMEOUT";
    private static final String TIMEOUT_I18N = "challenge.status.timeout";

    @FXML
    private AnchorPane parentObject;

    @FXML
    private TextField player1Text;

    @FXML
    private Button btn1;

    @FXML
    private Label player1Status;

    @FXML
    private TextField player2Text;

    @FXML
    private Button btn2;

    @FXML
    private Label player2Status;

    @FXML
    private TextField player3Text;

    @FXML
    private Button btn3;

    @FXML
    private Label player3Status;

    @FXML
    private Button abortChallenge;

    @FXML
    private Button startGame;

    @FXML
    private Label gameStatus;

    /**
     * Initializes the client and resource local variables
     * @param c Client on which this controller runs
     * @param res ResourceBundle with language strings to use
     */
    public ChallengeController(Client c, ResourceBundle res) {
        this.client = c;
        this.resource = res;
    }

    /**
     * Standard initialize() function
     * Contacts server and sets up the set-up process for a challenge game
     */
    @FXML
    public void initialize() {
        this.gameStatus.setText(this.resource.getString("challenge.status.contact"));
        this.client.setChallengeController(this);

        //Send command to server and wait for response
        String response = this.client.getClient().sendCommandAndWait("ChallengeStart");

        if ("ChallengeSetUpSuccessful".equals(response)) {
            this.gameStatus.setText(this.resource.getString("challenge.status.contactsuccess"));
        }
        if ("TooManyChallenges".equals(response)) {
            this.gameStatus.setText(this.resource.getString("challenge.status.contactfailtoomany"));
        }
        if (TIMEOUT_RESPONSE.equals(response)) {
            this.gameStatus.setText(this.resource.getString(TIMEOUT_I18N));
        }

    }

    /**
     * Challenge a player
     * @param e MouseEvent on the button next to the textbox with the name of the player to be challenged
     */
    @FXML
    public void challengePlayer(MouseEvent e) {
        int buttonNumber = this.pressedButton(e);
        TextField selectedText = this.selectedTextField(buttonNumber);
        Label selectedStatus = this.selectedStatus(buttonNumber);

        //Disable editing on textbox, send command to server to challenge player
        selectedText.setEditable(false);
        selectedStatus.setText(this.resource.getString("challenge.status.contact"));
        String response = this.client.getClient().sendCommandAndWait("ChallengePlayer&" + selectedText.getText());

        if ("ChallengeSent".equals(response)) {
            selectedStatus.setText(this.resource.getString("challenge.status.challengesent"));
        }
        if ("PlayerNotFound".equals(response)) {
            selectedStatus.setText(this.resource.getString("challenge.status.playernotfound"));
            selectedText.setEditable(true);
        }
        if (TIMEOUT_RESPONSE.equals(response)) {
            selectedStatus.setText(this.resource.getString(TIMEOUT_I18N));
            selectedText.setEditable(true);
        }

    }

    /**
     * Returns the row number of the pressed button
     * @param e MouseEvent on which button was pressed
     * @return Row number, or 0 if not found
     */
    private int pressedButton(MouseEvent e) {
        Button temp = (Button) e.getSource();

        if (temp == btn1) {
            return 1;
        } else if (temp == btn2) {
            return 2;
        } else if (temp == btn3) {
            return 3;
        } else {
            return 0;            
        }
    }

    private int updateRowSelect(String playerName) {
        if (playerName.equals(this.player1Text.getText())) {
            return 1;
        } else if (playerName.equals(this.player2Text.getText())) {
            return 2;
        } else if (playerName.equals(this.player3Text.getText())) {
            return 3;
        } else {
            return 0;
        }

    }

    /**
     * TextField on the number row
     * @param number Row number to pull the TextField from
     * @return TextField that was selected
     */
    private TextField selectedTextField(int number) {
        if (number == 1) {
            return this.player1Text;
        } else if (number == 2) {
            return this.player2Text;
        } else if (number == 3) {
            return this.player3Text;
        } else {
            return null;    
        }
    }

    /**
     * Status on the number row
     * @param number Row number to pull the Label from
     * @return Label that was selected
     */
    private Label selectedStatus(int number) {
        if (number == 1) {
            return this.player1Status;
        } else if (number == 2) {
            return this.player2Status;
        } else if (number == 3) {
            return this.player3Status;
        } else {
            return null;   
        }
    }

    /**
     * Receives a challenge update from the server
     * @param playerName Name of the player who is being updated
     * @param update Update as String
     */
    public void receiveChallengeUpdate(String playerName, String update) {
        int player = updateRowSelect(playerName);
        TextField selectedText = this.selectedTextField(player);
        Label selectedStatus = this.selectedStatus(player);

        if ("Accept".equals(update)) {
            selectedStatus.setText(this.resource.getString("challenge.status.challengeaccept"));
        }
        if ("Denied".equals(update)) {
            selectedStatus.setText(this.resource.getString("challenge.status.challengedenied"));
            selectedText.setEditable(true);
        }
        if ("Disconnect".equals(update)) {
            selectedStatus.setText(this.resource.getString("challenge.status.challengedisconnect"));
            selectedText.setEditable(true);
        }

    }

    /**
     * Send a abort challenge request to the server
     * @param e MouseEvent on the pressed button
     */
    @FXML
    public void abortChallenge(MouseEvent e) {
        String response = this.client.getClient().sendCommandAndWait("ChallengeAbort");

        if ("ChallengeAbortSuccess".equals(response)) {
            this.gameStatus.setText(this.resource.getString("challenge.status.abortsuccess"));
            this.client.setChallengeController(null);
        }
        if ("ChallengeAbortFail".equals(response)) {
            this.gameStatus.setText(this.resource.getString("challenge.status.abortfail"));
        }
        if (TIMEOUT_RESPONSE.equals(response)) {
            this.gameStatus.setText(this.resource.getString(TIMEOUT_I18N));
        }
    }

    /**
     * Attempt to start a game with the players that accepted the challenge
     * @param e MouseEvent on the pressed button
     */
    @FXML
    public void attemptStart(MouseEvent e) {
        String response = this.client.getClient().sendCommandAndWait("ChallengeStartGame");

        if ("ErrorNotEnoughPlayers".equals(response)) {
            this.gameStatus.setText(this.resource.getString("challenge.status.startnotenoughplayers"));
        }
        if ("GameStartSuccess".equals(response)) {
            this.gameStatus.setText(this.resource.getString("challenge.status.startsuccess"));
        }
        if ("GameStartFail".equals(response)) {
            this.gameStatus.setText(this.resource.getString("challenge.status.startfail"));
        }
        if (TIMEOUT_RESPONSE.equals(response)) {
            this.gameStatus.setText(this.resource.getString(TIMEOUT_I18N));
        }

    }

}
