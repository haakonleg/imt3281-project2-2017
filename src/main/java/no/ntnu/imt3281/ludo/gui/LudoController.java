package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;

import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.Chat;

/**
 * Controller for Ludo.fxml
 * 
 * @author Hakkon
 *
 */
public class LudoController {
    private static final Logger LOGGER = Logger.getLogger(LudoController.class.getName());
    
    @FXML
    private MenuItem random;

    @FXML
    private TabPane tabbedPane;
    
    @FXML
    private Label usernameLabel;

    private Client client;
    private ResourceBundle resource;
    private Gson gson;

    /**
     * Controller constructor. Gets a reference to ClientThread object.
     * 
     * @param client ClientThread object
     * @param res ResourceBundle with language files 
     */
    public LudoController(Client client, ResourceBundle res) {
        this.client = client;
        this.resource = res;
        this.gson = new Gson();

        this.client.setLudoController(this);
    }

    /**
     * Joins the global chat room
     */
    public void initialize() {
        this.joinChat("GlobalChat");
        this.usernameLabel.setText(this.resource.getString("ludo.loggedinas") + " " + this.client.getClient().getUser().getName());
        this.tabbedPane.setTabClosingPolicy(TabClosingPolicy.SELECTED_TAB);
    }
    
    private void showDialog(String title, String text, AlertType type) {
        Alert dialog = new Alert(type);
        dialog.setTitle(title);
        dialog.setHeaderText(null);
        dialog.setContentText(text);
        dialog.showAndWait();
        dialog = null;
    }

    /**
     * Creates a new tab and adds it to the tabbed pane
     * 
     * @param fxml
     *            FXML Resource url
     * @param controller
     *            The javafx controller object
     * @param tabName
     *            Name of the new tab
     */
    private void newTab(String fxml, Object controller, String tabName) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));

        loader.setController(controller);
        if (this.resource != null) {
            loader.setResources(this.resource);
        }
        
        try {
            AnchorPane pane = loader.load();
            Tab tab = new Tab(tabName);
            tab.setContent(pane);
            this.tabbedPane.getTabs().add(tab);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error adding new tabbed pane", e);
        }
    }

    private void newWindow(String fxml, Object controller, String title) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));

        loader.setController(controller);
        if (this.resource != null) {
            loader.setResources(this.resource);
        }
        
        try {
            Stage stage = new Stage();
            AnchorPane pane = loader.load();
            Scene scene = new Scene(pane);
            stage.setScene(scene);
            stage.setTitle(title);
            stage.setResizable(false);
            stage.sizeToScene();
            stage.show();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error adding new window", e);
        }
    }
    
    @FXML
    void close(ActionEvent event) {
        this.client.close();
    }
    
    @FXML
    void about(ActionEvent event) {
        this.showDialog("Hallo", "Her er det ingenting å se", AlertType.INFORMATION);
    }

    /**
     * Joins a chat room. A new chat room is added to the tabbed pane. If the chat
     * room does not exist, an error dialog is shown.
     * 
     * @param chatID
     *            The chatID of the chat room to join.
     */
    public void joinChat(String chatID) {
        String response = client.getClient().sendCommandAndWait("ChatEvent&" + chatID + "&JOIN");

        if ("NO_SUCH_CHAT".equals(response)) {
            this.showDialog(this.resource.getString("error"), this.resource.getString("ludo.joinchaterror.nochat"), AlertType.ERROR);
        } else if ("ALREADY_JOINED".equals(response)) {
            this.showDialog(this.resource.getString("error"), this.resource.getString("ludo.joinchaterror.alreadyjoined"), AlertType.ERROR);
        } else {
            // Get Chat object from server and add to client ChatManager
            Chat chat = gson.fromJson(response, Chat.class);
            String id = this.client.getChatManager().addChat(chat);

            ChatController controller = new ChatController(this.client, this.client.getChatManager().getChat(id), false, this.resource);
            this.newTab("Chat.fxml", controller, chat.getName());

            Tab tab = this.tabbedPane.getTabs().get(this.tabbedPane.getTabs().size() - 1);

            // Leave chat on tab close
            tab.setOnCloseRequest(e -> {
                this.client.getChatManager().removeChat(chat.getID());
                client.getClient().sendCommand("ChatEvent&" + chatID + "&LEAVE");
            });
        }
    }

    @FXML
    void listChatRooms(ActionEvent event) {
        ChatListController controller = new ChatListController(this.client, this);
        this.newWindow("ChatList.fxml", controller, this.resource.getString("chatlist.headertitle"));
    }

    @FXML
    void joinRandomGame(ActionEvent e) {
        Alert startRandom = new Alert(AlertType.INFORMATION);
        startRandom.setTitle(this.resource.getString("ludo.joinrandom.title"));
        startRandom.setHeaderText(null);
        startRandom.setContentText(this.resource.getString("ludo.joinrandom.connecting"));
        startRandom.show();

        String response = this.client.getClient().sendCommandAndWait("QueueRandom");

        if ("ADDED".equals(response)) {
            startRandom.setContentText(this.resource.getString("ludo.joinrandom.added"));
        } else if ("NOT_ADDED".equals(response)) {
            startRandom.setContentText(this.resource.getString("ludo.joinrandom.notadded"));
        } else if ("TIMEOUT".equals(response)) {
            startRandom.setContentText(this.resource.getString("ludo.joinrandom.timeout"));
        }
    }

    /**
     * Pop-up window with accept-decline answers to a challenge issued by the challenger
     * @param challenger Name of the challenger
     */
    public void challengeIssued(String challenger) {
        Alert challenge = new Alert(AlertType.CONFIRMATION);
        challenge.setTitle(this.resource.getString("ludo.challenge.title"));
        challenge.setContentText(this.resource.getString("ludo.challenge.text") + " " + challenger + "!");

        ButtonType buttonAccept = new ButtonType(this.resource.getString("ludo.challenge.accept"));
        ButtonType buttonDecline = new ButtonType(this.resource.getString("ludo.challenge.decline"));
        challenge.getButtonTypes().setAll(buttonAccept, buttonDecline);
        Optional<ButtonType> answer = challenge.showAndWait();
        if (answer.get() == buttonAccept) {
            this.client.getClient().sendCommand("ChallengeResponse&" + challenger + "&ACCEPT");
            return;
        }
        // Unless client presses accept, default for any other action is deny
        this.client.getClient().sendCommand("ChallengeResponse&" + challenger + "&DENIED");

    }

    /**
     * Open a challenge tab, challenging players to a game of Ludo
     * @param e ActionEvent on mouse press
     */
    @FXML
    public void startChallenge(ActionEvent e) {
        ChallengeController controller = new ChallengeController(this.client, this.resource);
        this.newTab("ChallengeWindow.fxml", controller, "Challenge");
    }

    /**
     * Open a new Game tab, with a game set up with param values
     * @param red Name of the red player
     * @param blue Name of the blue player
     * @param yellow Name of the yellow player
     * @param green Name of the green player
     * @param id Unique ID of the game
     */
    public void startRandomGame(String red, String blue, String yellow, String green, String id) {
        GameBoardController controller = new GameBoardController(red, blue, yellow, green, id, this.client, this.resource);
        this.client.addGameController(controller);
        this.newTab("GameBoard.fxml", controller, "Game");
    }
}
