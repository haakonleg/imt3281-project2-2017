package no.ntnu.imt3281.ludo.gui;
/**
 * Sample Skeleton for 'GameBoard.fxml' Controller Class
 */

import java.io.IOException;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.common.Chat;
import no.ntnu.imt3281.ludo.logic.DiceEvent;
import no.ntnu.imt3281.ludo.logic.PieceEvent;
import no.ntnu.imt3281.ludo.logic.PlayerEvent;

/**
 * Controller of the GameBoard.fxml, responsible for managing the game
 * @author Maciek
 *
 */
public class GameBoardController {
    private static final Logger LOGGER = Logger.getLogger(GameBoardController.class.getName());

    private String gameID;
    private String player1;
    private String player2;
    private String player3;
    private String player4;
    private Client client;
    private ResourceBundle resource;

    private MouseEvent step1;
    private DiceEvent lastDice;

    @FXML
    private Pane board;
    
    @FXML
    private Pane chatPane;

    @FXML
    private Label player1Name;

    @FXML
    private ImageView player1Active;

    @FXML
    private Label player2Name;

    @FXML
    private ImageView player2Active;

    @FXML
    private Label player3Name;

    @FXML
    private ImageView player3Active;

    @FXML
    private Label player4Name;

    @FXML
    private ImageView player4Active;

    @FXML
    private ImageView diceThrown;

    @FXML
    private Button throwTheDice;

    @FXML
    private TextArea chatArea;

    @FXML
    private TextField textToSay;

    @FXML
    private Button sendTextButton;

    /**
     * Initializes the local variables to parameter values
     * @param red Name of the Red player
     * @param blue Name of the Blue player
     * @param yellow Name of the Yellow player
     * @param green Name of the Green player
     * @param id Unique ID of the game
     * @param c Client on which the controller runs
     * @param res ResourceBundle with the language files
     */
    public GameBoardController(String red, String blue, String yellow, String green, String id, Client c, ResourceBundle res) {
        this.gameID = id;
        this.client = c;
        this.resource = res;

        if (red != null && !"null".equals(red)) {
            this.player1 = red;
        }
        if (blue != null && !"null".equals(blue)) {
            this.player2 = blue;
        }
        if (yellow != null && !"null".equals(yellow)) {
            this.player3 = yellow;
        }
        if (green != null && !"null".equals(green)) {
            this.player4 = green;
        }
    }
    
    /**
     * Standard FXML initialize() function
     * Initializes the GUI to the earlier-set values
     */
    @FXML
    public void initialize() {
        if (this.player1 != null) {
            this.player1Name.setText(this.player1);
        } else {
            this.player1Name.setText("");
            this.player1Name.setVisible(false);
        }
            
        if (this.player2 != null) {
            this.player2Name.setText(this.player2);
        } else {
            this.player2Name.setText("");
            this.player2Name.setVisible(false);
        }
        if (this.player3 != null) {
            this.player3Name.setText(this.player3);
        } else {
            this.player3Name.setText("");
            this.player3Name.setVisible(false);
        }
        if (this.player4 != null) {
            this.player4Name.setText(this.player4);
        } else {
            this.player4Name.setText("");
            this.player4Name.setVisible(false);
        }
        
        // Initialize chat pane
        this.initChat();
    }
    
    /**
     * Initializes the chat in GameBoard.fxml
     */
    private void initChat() {
        Gson gson = new Gson();
        
        String response = client.getClient().sendCommandAndWait("ChatEvent&" + this.gameID + "&JOIN");
        Chat chat = gson.fromJson(response, Chat.class);
        String id = this.client.getChatManager().addChat(chat);
        ChatController controller = new ChatController(this.client, this.client.getChatManager().getChat(id), true, this.resource);
        

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ChatPane.fxml"));
            loader.setResources(this.resource);
            loader.setController(controller);
            Pane pane = loader.load();
            this.chatPane.getChildren().add(pane);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Could not load chat pane", e);
        }
    }

    /**
     * Sends a command to server to throw the dice
     */
    @FXML
    protected void throwDice() {
        this.client.getClient().sendCommand("FromCThrowDice&" + this.gameID);
    }
    
    /**
     * Receives a DiceEvent from server, updates the dice image accordingly
     * @param diceEvent DiceEvent from server
     */
    public void updateDice(DiceEvent diceEvent) {
        this.lastDice = diceEvent;
        Image newImg = new Image(getClass().getResource("..\\..\\..\\..\\..\\images\\dice" + diceEvent.getDice() + ".png").toExternalForm());
        this.diceThrown.setImage(newImg);
    }

    /**
     * Attempts to move a piece on the board
     * @param e MouseEvent on the pressed board location
     */
    @FXML
    protected void attemptMove(MouseEvent e) {
        //If the first step (from) isn't set, set it and quit
        if (this.step1 == null) {
            this.step1 = e;
            return;
        }
        
        //If no dice has been thrown yet
        if (this.lastDice == null) {
            return;
        }
        
        int source = this.attemptDiceThrowToInt(((Pane)this.step1.getSource()).getId());
        int target = this.attemptDiceThrowToInt(((Pane)e.getSource()).getId());
        
        if (source == -1 || target == -1) {
            return;
        }
        
        //Send command to server to move piece
        this.client.getClient().sendCommand("FromCMovePiece&" + this.gameID + "&" + source + "&" + target);
        
        //Reset step 1
        this.step1 = null;
    }

    /**
     * Receive a PieceEvent from server, move piece according to data in PieceEvent
     * @param move PieceEvent from server
     */
    public void movePiece(PieceEvent move) {
        String s = this.stringFromPieceEvent(move, true);
        String t = this.stringFromPieceEvent(move, false);
        
        //Set source Pane
        Pane source = null;
        if (move.getFrom() != 0) {
            source = this.getPane(s);
        } else {
            source = this.firstOccupied(move);
        }

        //Set target pane
        Pane target = null;
        if (move.getTo() != 0) {
            target = this.getPane(t);
        } else {
            target = this.firstFree(move);
        }

        //If neither are null, and source has a piece that can be moved
        if (source != null && target != null && !source.getChildren().isEmpty()) {
            //Remove top piece from source
            Group temp = (Group) source.getChildren().remove(0);
            
            //If target already has a piece, hide this piece, otherwise reveal it
            if (!target.getChildren().isEmpty()) {
                ((ImageView) temp.getChildren().get(0)).setOpacity(0);
            } else {
                ((ImageView) temp.getChildren().get(0)).setOpacity(1);
            }

            //If source is not empty, reveal the current top piece
            if (!source.getChildren().isEmpty()) {
                ((ImageView) source.getChildren().get(0)).setOpacity(1);
            }

            //Add the selected piece to target
            target.getChildren().add(temp);
        }

    }

    /**
     * Looks for the first free Pane (without piece), returns it
     * @param move PieceEvent that requires a free Pane
     * @return Pane without children, null if none found
     */
    private Pane firstFree(PieceEvent move) {
        for (int i = 0; i <= 3; i++) {
            Pane temp = this.getPane(this.stringFromPieceEvent(move, true, i));
            if (temp.getChildren().isEmpty()) {
                return temp;
            }
        }

        return null;
    }

    /**
     * Looks for the first occupied Pane (with piece), returns it
     * @param move PieceEvent that requires a occupied Pane
     * @return Pane with children, null if none found
     */
    private Pane firstOccupied(PieceEvent move) {
        for (int i = 0; i <= 3; i++) {
            Pane temp = this.getPane(this.stringFromPieceEvent(move, false, i));
            if (!temp.getChildren().isEmpty()) {
                return temp;
            }
        }

        return null;
    }

    /**
     * Receives a PlayerEvent from server, adjusts values accordingly
     * @param change PlayerEvent from server
     */
    public void playerStatusChange(PlayerEvent change) {
        int playerId = change.getActivePlayer();

        ImageView dice = null;
        Label player = null;
        switch (playerId) {
        case 0:
            dice = this.player1Active;
            player = this.player1Name;
            break;
        case 1:
            dice = this.player2Active;
            player = this.player2Name;
            break;
        case 2:
            dice = this.player3Active;
            player = this.player3Name;
            break;
        case 3:
            dice = this.player4Active;
            player = this.player4Name;
            break;
        default:
            return;
        }

        if (dice != null) {
            if (change.getState() == PlayerEvent.WAITING) {
                dice.setOpacity(0);
            }
            if (change.getState() == PlayerEvent.PLAYING) {
                dice.setOpacity(1);
            }
            if (change.getState() == PlayerEvent.LEFTGAME) {
                dice.setOpacity(0);
                player.setText("****" + player.getText());
            }

        }

    }

    /**
     * Returns the gameID of the game running on this controller
     * @return gameID
     */
    public String getGameID() {
        return this.gameID;
    }
    
    /**
     * Looks for, and returns the pane containing s as part of its id
     * @param s Part of id of the Pane to be returned
     * @return Pane if one such Pane was found, null if not
     */
    private Pane getPane(String s) {
        for (Object object : this.board.getChildren()) {
            if (object instanceof Pane) {
                Pane temp = (Pane) object;
                if (temp.getId().contains(s)) {
                    return temp;
                }
            }
        }
        return null;
    }
    
    /**
     * Returns part of Pane id as String based on a PieceEvent, needed to find the Pane to move
     * @param move PieceEvent on which to base the String
     * @param from TRUE if looking for the FROM location, FALSE if looking for the TO location
     * @return Part of Pane id as String
     */
    private String stringFromPieceEvent(PieceEvent move, boolean from) {
        return stringFromPieceEvent(move, from, -1);
    }
    
    /**
     * Returns part of Pane id as String based on a PieceEvent, needed to find the Pane to move
     * @param move PieceEvent on which to base the String
     * @param from TRUE if looking for the FROM location, FALSE if looking for the TO location
     * @param i Sub-id if looking for the 0th coordinate, -1 if not necessary
     * @return Part of Pane id as String
     */
    private String stringFromPieceEvent(PieceEvent move, boolean from, int i) {
        int playerId = move.getPlayer();
        String s = "";
        
        switch (playerId) {
        case 0:
            s = "red";
            break;
        case 1:
            s = "blue";
            break;
        case 2:
            s = "yellow";
            break;
        case 3:
            s = "green";
            break;
        default:
            s = "common";
            break;
        }
        
        //If the move is not on position 0
        if (i == -1) {
            if (from) {
                s += move.getFrom() + ",";
            } else {
                s += move.getTo() + ",";
            }
        } else {
            s += "0_" + i + ",";
        }
        
        return s;
    }
    
    /**
     * Int coordinate of the location that was clicked on, location being based on last thrown dice
     * @param id Full String id of the clicked pane
     * @return id as local coordinate for the last thrown player, -1 if it couldn't be found
     */
    private int attemptDiceThrowToInt(String id) {
        String s = id;
        int playerId = this.lastDice.getPlayer();

        String playerColor = "";
        switch (playerId) {
        case 0:
            playerColor = "red";
            break;
        case 1:
            playerColor = "blue";
            break;
        case 2:
            playerColor = "yellow";
            break;
        case 3:
            playerColor = "green";
            break;
        default:
            return -1;
        }
        
        //If the playerColor can't be found as part if s, return -1
        if (s.indexOf(playerColor) == -1) {
            return -1;
        }
        
        //Cut up the string to only contain the number after the playerColor in s (..red1,..) -> (1)
        s = s.substring(s.indexOf(playerColor) + playerColor.length(), s.length());
        s = s.substring(0, s.indexOf(","));
        if (s.indexOf("_") != -1) {
            s = s.substring(0, s.indexOf("_"));
        }
        
        return Integer.parseInt(s);
    }
}