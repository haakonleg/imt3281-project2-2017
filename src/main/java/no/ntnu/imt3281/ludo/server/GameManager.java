package no.ntnu.imt3281.ludo.server;

import java.util.ArrayList;

import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

import no.ntnu.imt3281.ludo.common.Chat;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ClientThread;
import no.ntnu.imt3281.ludo.gui.GameBoardController;
import no.ntnu.imt3281.ludo.logic.DiceEvent;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.LudoDiceListener;
import no.ntnu.imt3281.ludo.logic.LudoPieceListener;
import no.ntnu.imt3281.ludo.logic.LudoPlayerListener;
import no.ntnu.imt3281.ludo.logic.PieceEvent;
import no.ntnu.imt3281.ludo.logic.PlayerEvent;


/**
 * Class responsible for starting, and maintaining games on the server.
 * @author Maciek
 *
 */
public class GameManager {
    private static final Logger LOGGER = Logger.getLogger(GameBoardController.class.getName());
    
    //Set between 2 (lowest player count in a game) and 4 (full game)
    private static final int LOWEST_RANDOM_PLAYER_START = 4;
    
    private Thread randomGameStarterThread;
    
    private ArrayList<Ludo> activeGames;
    private ArrayList<ArrayList<String>> activeGamesUsers;
    private ArrayList<String> activeGamesID;
    private ArrayList<String> randomQueue;
    private ArrayList<ArrayList<String>> challengeGameSetup;
    private Semaphore randomQueueAccess;
    private Gson gson;
    
    private ClientManager clientManager;
    private ChatManager chatManager;

    /**
     * Initialization class for GameManager, sets all local variables to usable state
     * @param cm The ClientManager object on the server
     * @param chatManager The ChatManager object on the server
     */
    public GameManager(ClientManager cm, ChatManager chatManager) {
        this.randomGameStarterThread = new Thread() {
            public void run() {
                    randomGameStarter();
            }
        };
        
        this.activeGames = new ArrayList<>();
        this.activeGamesUsers = new ArrayList<>();
        this.activeGamesID = new ArrayList<>();
        this.randomQueue = new ArrayList<>();
        this.challengeGameSetup = new ArrayList<>();
        this.randomQueueAccess = new Semaphore(1, true);
        this.gson = new Gson();
        
        this.clientManager = cm;
        this.chatManager = chatManager;
        
        //Start randomly creating games as players join the queue
        this.startRandomGameStarter();
    }

    /**
     * Adds the client username in parameter to random game queue
     * @param client Client to add to random queue as String
     * @return  TRUE if player is added, FALSE if player is not added
     */
    public boolean addToRandomQueue(String client) {
        this.randomQueueAccess.acquireUninterruptibly();
        for (int i = 0; i < this.randomQueue.size(); i++) {
            if (this.randomQueue.get(0).equals(client)) {
                this.randomQueueAccess.release();
                return false;
            }
        }
        
        boolean ret = this.randomQueue.add(client);  
        this.randomQueueAccess.release();
        return ret;
    }

    /**
     * Function responsible for managing random game queue and starting random games
     * Start execution with startRandomGameStarter()
     * Stop execution with stopRandomGameStarter()
     */
    private void randomGameStarter() {
        //While the thread is not interrupted
        while (!this.randomGameStarterThread.isInterrupted()) {
            this.randomQueueAccess.acquireUninterruptibly();
            ArrayList<String> game = new ArrayList<>();
            
            //While there is something to get from queue, and game isn't full
            while (!this.randomQueue.isEmpty() && game.size() < 4) {
                if (clientManager.getClient(this.randomQueue.get(0)) != null) {
                    game.add(this.randomQueue.remove(0));
                }
            }
            
            // If there were less than 2 players in random queue that were still active
            if (game.size() < 2) {
                this.randomQueue.addAll(game);
            } else if (!this.startGame(game)) {
                // Game failed to start
                this.randomQueue.addAll(game);
            }
            
            //Save remaining players in queue as int, and release access for other functions
            int remainingInQueue = this.randomQueue.size();
            this.randomQueueAccess.release();
            
            //If there are less players in queue than a single full game, freeze thread for 5000ms
            if (remainingInQueue < LOWEST_RANDOM_PLAYER_START) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    LOGGER.log(Level.SEVERE, "randomGameStarter exception", e);
                }
            }
        }
    }

    /**
     * Starts the random game starter thread
     * WARNING: randomGameStarterThread has to be initialized first
     */
    public void startRandomGameStarter() {
        this.randomGameStarterThread.start();
    }

    /**
     * Stops the random game starter thread
     */
    public void stopRandomGameStarter() {
        this.randomGameStarterThread.interrupt();
    }

    /**
     * Adds one of each of the three ludo listeners to a created game.
     * @param ludo Ludo game to add the listeners to
     * @param id Unique generated ID as String
     */
    private void addListenersToGame(Ludo ludo, String id) {
        LudoDiceListener controllerDiceListener = new LudoDiceListener();
        LudoPieceListener controllerPieceListener = new LudoPieceListener();
        LudoPlayerListener controllerPlayerListener = new LudoPlayerListener();

        ludo.addDiceListener(controllerDiceListener);
        ludo.addPieceListener(controllerPieceListener);
        ludo.addPlayerListener(controllerPlayerListener);

        controllerDiceListener.linkGameManager(this, id);
        controllerPieceListener.linkGameManager(this, id);
        controllerPlayerListener.linkGameManager(this, id);
    }

    /**
     * Receives a request from a client to throw a dice in a game
     * @param client Client requesting to throw a dice
     * @param id ID of the game the player wants to throw a dice in
     */
    public void receiveDiceThrow(ClientThread client, String id) {
        int position = this.activeGamesID.indexOf(id);

        //If the game exists, and the player attempting to throw the dice is the one with the turn to do so, throw dice
        if (position != -1 && this.activeGames.get(position).activePlayer() == 
                this.activeGamesUsers.get(position).indexOf(client.getUser().getName())) {
            this.activeGames.get(position).throwDice();
        }
    }

    /**
     * Receives a DiceEvent, and sends it forward to the clients in a particular game
     * @param dice DiceEvent sent by a game through an event listener
     * @param id Unique ID generated during a games creation
     */
    public void sendDiceToUsers(DiceEvent dice, String id) {
        int position = this.activeGamesID.indexOf(id);
        String diceEventAsJson = gson.toJson(dice, DiceEvent.class);

        //If a game exists, send the event to all players in that game
        if (position != -1) {
            for (int i = 0; i < this.activeGamesUsers.get(position).size(); i++) {
                ClientThread client = this.clientManager.getClient(this.activeGamesUsers.get(position).get(i));
                if (client != null) {
                    client.sendCommand("FromSDiceEvent&" + diceEventAsJson + "&" + id);
                }
            }
        }
    }

    /**
     * Receives a request to move a piece from the client
     * @param client Client requesting the move
     * @param id ID of the game the client is requesting a move in
     * @param from Location from which the client wants to move a piece
     * @param to Location to which the client wants to move a piece
     */
    public void receiveMovePiece(ClientThread client, String id, int from, int to) {
        int position = this.activeGamesID.indexOf(id);
        
        //If the game exists, and the player attempting to move has the turn, try to make the move
        if (position != -1 && 
                this.activeGames.get(position).activePlayer() == 
                this.activeGamesUsers.get(position).indexOf(client.getUser().getName()) &&
                to != 0) {
            this.activeGames.get(position).movePiece(
                    this.activeGamesUsers.get(position).indexOf(client.getUser().getName()), from, to);
        }
    }

    /**
     * Receives a PieceEvent, sends it forward to clients in a particular game
     * @param piece PieceEvent moving a piece on the board
     * @param id ID of the game to which the PieceEvent belongs to
     */
    public void sendMoveToUsers(PieceEvent piece, String id) {
        int position = this.activeGamesID.indexOf(id);
        String pieceEventAsJson = gson.toJson(piece, PieceEvent.class);

        //If the game exists, send the event to all clients
        if (position != -1) {
            for (int i = 0; i < this.activeGamesUsers.get(position).size(); i++) {
                ClientThread client = this.clientManager.getClient(this.activeGamesUsers.get(position).get(i));
                if (client != null) {
                    client.sendCommand("FromSMovePiece&" + pieceEventAsJson + "&" + id);
                }
            }
        }
    }

    /**
     * Receives a PlayerEvent, and sends it forward to clients in a particular game
     * @param player PlayerEvent registering a change in player state
     * @param id ID of the game the event belongs to
     */
    public void sendPlayerToUsers(PlayerEvent player, String id) {
        int position = this.activeGamesID.indexOf(id);
        String playerEventAsJson = gson.toJson(player, PlayerEvent.class);

        //If the game exists, send the event to all clients
        if (position != -1) {
            for (int i = 0; i < this.activeGamesUsers.get(position).size(); i++) {
                ClientThread client = this.clientManager.getClient(this.activeGamesUsers.get(position).get(i));
                if (client != null) {
                    client.sendCommand("FromSPlayerEvent&" + playerEventAsJson + "&" + id);
                }
            }
        }
    }

    /**
     * Start a challenge game set-up process, letting the client challenge other players to a game of Ludo
     * @param client Client that wants to challenge other players
     */
    public void startChallenge(ClientThread client) {
        ArrayList<String> game = challengeGameReturn(client.getUser().getName());

        //If the user already has a challenge game in set-up process
        if (!game.isEmpty()) {
            client.sendResponse("TooManyChallenges");
            return;
        }

        ArrayList<String> temp = new ArrayList<>();
        temp.add(client.getUser().getName());
        this.challengeGameSetup.add(temp);
        client.sendResponse("ChallengeSetUpSuccessful");
    }

    /**
     * Search for a challenge game set-up by the player who is hosting the challenge
     * @param host Host of the challenge as String
     * @return ArrayList<String> with all players currently in that challenge
     */
    private ArrayList<String> challengeGameReturn(String host) {
        for (int i = 0; i < this.challengeGameSetup.size(); i++) {
            if (this.challengeGameSetup.get(i).get(0).equals(host)) {
                return this.challengeGameSetup.get(i);
            }

        }

        return new ArrayList<>();
    }

    /**
     * Send a challenge to another player
     * @param client Client of the player challenging the target
     * @param challengeTarget Player who is challenged, as String
     */
    public void challengePlayerSend(ClientThread client, String challengeTarget) {
        ArrayList<String> game = challengeGameReturn(client.getUser().getName());

        //If the challenge game does not exist
        if (game.isEmpty()) {
            client.sendResponse("ErrorNoChallenge");
            return;
        }

        //Go through all clients connected to server, if username match is found, send challenge and quit function
        for (int j = 0; j < this.clientManager.nrOfClients(); j++) {
            if (this.clientManager.getClient(j).getUser().getName().equals(challengeTarget)) {
                this.clientManager.getClient(j).sendCommand("ChallengeIssued&" + client.getUser().getName());
                client.sendResponse("ChallengeSent");
                return;
            }
        }

        //If no player was found in the client manager with that name
        client.sendResponse("PlayerNotFound");
    }

    /**
     * Receives a response from challenge target whether or not he accepted the challenge, or if the client disconnected
     * @param client Client of the challenge target
     * @param challengeSource Host of the challenge as String
     * @param response Response to the challenge as String
     */
    public void challengePlayerReceive(ClientThread client, String challengeSource, String response) {
        ArrayList<String> game = challengeGameReturn(challengeSource);
        String responseToCommand = "";

        //If the challenge does not exist, quit
        if (game.isEmpty()) {
            return;
        }

        if ("ACCEPT".equals(response)) {
            game.add(client.getUser().getName());
            responseToCommand = "Accept";
        } else if ("Disconnect".equals(response)) {
            game.remove(client.getUser().getName());
            responseToCommand = response;
        } else {
            responseToCommand = "Denied";
        }

        //Look for host in client manager, send him the response
        for (int j = 0; j < this.clientManager.nrOfClients(); j++) {
            if (this.clientManager.getClient(j).getUser().getName().equals(challengeSource)) {
                this.clientManager.getClient(j)
                        .sendCommand("ChallengeResponse&" + client.getUser().getName() + "&" + responseToCommand);
            }
        }
    }

    /**
     * Abort the challenge game set-up process
     * @param client Client wanting to abort the process
     */
    public void challengeAbort(ClientThread client) {
        ArrayList<String> game = challengeGameReturn(client.getUser().getName());

        //If the client has a game to remove, remove it, send response depending on whether it was removed
        if (this.challengeGameSetup.remove(game)) {
            client.sendResponse("ChallengeAbortSuccess");
        } else {
            client.sendResponse("ChallengeAbortFail");
        }
    }

    /**
     * Start a game based on the challenge game that was set up
     * @param client Client that wants to start the challenge game (host)
     */
    public void challengeStartGame(ClientThread client) {
        ArrayList<String> game = challengeGameReturn(client.getUser().getName());

        //If the player has no challenge game setup going
        if (game.isEmpty()) {
            client.sendResponse("ErrorNoChallenge");
            return;
        }

        //If the game doesn't have enough players
        if (game.size() < 2) {
            client.sendResponse("ErrorNotEnoughPlayers");
            return;
        }

        //Try to start the game, if successful, remove set-up from memory
        if (startGame(game)) {
            client.sendResponse("GameStartSuccess");
            this.challengeGameSetup.remove(game);
        } else {
            client.sendResponse("GameStartFail");
        }

    }

    /**
     * Function responsible for starting Ludo games, both random and challenges
     * @param game ArrayList containing the usernames of players in the game to be started, MUST BE 2 or larger
     * @return TRUE if game started successfully, FALSE if not
     */
    private boolean startGame(ArrayList<String> game) {
        String player1 = game.get(0);
        String player2 = game.get(1);
        String player3 = null;
        String player4 = null;
        if (game.size() >= 3) {
            player3 = game.get(2);
        }
        if (game.size() >= 4) {
            player4 = game.get(3);
        }

        //Start the game and generate an ID for it
        Ludo ludo = new Ludo(player1, player2, player3, player4);
        String id = UUID.randomUUID().toString();

        //Add the game to arrays, and add listeners to ludo
        this.activeGames.add(ludo);
        this.activeGamesUsers.add(game);
        this.activeGamesID.add(id);
        this.addListenersToGame(ludo, id);
        
        // Create chat with same id as game
        Chat gameChat = new Chat("GameChat", id);
        this.chatManager.addChat(gameChat);

        //Once the game is set up, send the game information to players to start the game client-side
        for (int i = 0; i < game.size(); i++) {
            ClientThread client = this.clientManager.getClient(game.get(i));
            if (client != null) {
                client.sendCommand("StartGame&" + player1 + "&" + player2 + "&" + player3 + "&" + player4 + "&" + id);
            }
        }

        return true;
    }
    
    /**
     * Remove a client from games
     * @param client Client that is to be removed
     */
    public void removeUserFromGames(ClientThread client) {
        String username = client.getUser().getName();
        
        //Remove challenge user issued
        this.challengeGameSetup.remove(this.challengeGameReturn(username));
        
        //Remove from other's challenges
        for (ArrayList<String> challenges : this.challengeGameSetup) {
            for (int i = 1; i < challenges.size(); i++) {
                if (challenges.get(i).equals(username)) {
                    challenges.remove(i);
                    
                    //Send command to challenge host that this client disconnected
                    this.challengePlayerReceive(client, challenges.get(0), "Disconnect");
                }
            }
        }
        
        //Remove from random queue
        this.randomQueueAccess.acquireUninterruptibly();
        for (int randQueueRemove = 0; randQueueRemove < this.randomQueue.size(); randQueueRemove++) {
            while (this.randomQueue.get(randQueueRemove).equals(username)) {
                this.randomQueue.remove(username);
            }
        }
        this.randomQueueAccess.release();
        
        //Remove from games
        for (int j = 0; j < this.activeGames.size(); j++) {
            for (int k = 0; k < this.activeGamesUsers.get(j).size(); k++) {
                if (this.activeGamesUsers.get(j).get(k).equals(username) ) {
                    this.activeGames.get(j).removePlayer(username);
                    this.activeGamesUsers.get(j).remove(username);
                    //Events removing client from ludo game are afterwards handled by other functions
                }
            }
        }
    }
}
