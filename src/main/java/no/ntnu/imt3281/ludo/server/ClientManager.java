package no.ntnu.imt3281.ludo.server;

import java.io.IOException;


import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.command.ClientCommandInvoker;
import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * This class is responsible for managing connected clients.
 * 
 * @author Hakkon
 *
 */
public class ClientManager extends Thread {
    private static final Logger LOGGER = Logger.getLogger(ClientManager.class.getName());
    
    private ServerSocket serverSocket;
    private ArrayList<ClientThread> clients;
    private ClientDisconnectHandler disconnectHandler;
    private ClientCommandInvoker commandInvoker;

    /**
     * Constructor for ClientManager
     * 
     * @param serverSocket
     *            A reference to the server socket
     */
    public ClientManager(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
        this.clients = new ArrayList<>();
    }

    /**
     * Starts a thread that listens from client connections and adds/removes them in
     * a vector
     */
    @Override
    public void run() {

        LOGGER.info("Listening for clients on port " + Server.PORT);
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Socket newConnection = this.serverSocket.accept();

                ClientThread client = new ClientThread(newConnection, this.commandInvoker);
                client.setListener(this.disconnectHandler);

                LOGGER.info("Client connected from " + newConnection.getLocalAddress() + ":" + newConnection.getLocalPort());

                this.clients.add(client);
                client.start();

            } catch (IOException e) {
                LOGGER.log(Level.INFO, "ClientManager stopped", e);
            }
        }
    }

    /**
     * Closes the ClientManager, thread is interrupted and socket is closed.
     * 
     * @throws IOException
     *             Is thrown if socket could not be closed properly
     * @throws InterruptedException
     *             Is thrown if thread could not be interrupted
     */
    public void close() throws IOException {
        this.serverSocket.close();
        this.interrupt();
        for (ClientThread client : this.clients) {
            client.close();
        }
    }

    /**
     * Sets the CommandInvoker object, which is used for handling client commands
     * 
     * @param invoker
     *            Reference to ClientCommandInvoker object
     */
    public void setCommandInvoker(ClientCommandInvoker invoker) {
        this.commandInvoker = invoker;
    }

    /**
     * Sets the DisconnectHandler object, which is used for handling client
     * disconnects
     * 
     * @param disconnectHandler
     *            Reference to ClientDisconnectHandler object
     */
    public void setDisconnectHandler(ClientDisconnectHandler disconnectHandler) {
        this.disconnectHandler = disconnectHandler;
    }

    /**
     * Returns a Client by its index in the array
     * 
     * @param index
     *            The index of the Client object to return
     * @return Reference to the client object
     */
    public ClientThread getClient(int index) {
        return this.clients.get(index);
    }

    /**
     * Removes a Client object from the array
     * 
     * @param client
     *            Client object to remove
     * @return True if removed
     */
    public boolean removeClient(ClientThread client) {
        return this.clients.remove(client);
    }

    /**
     * Returns a Client object by the username associated with this Client
     * 
     * @param username
     *            String of the username
     * @return Reference to the client object if found, else null
     */
    public ClientThread getClient(String username) {
        for (ClientThread currClient : this.clients) {
            if (currClient.getUser() != null && currClient.getUser().getName().equals(username)) {
                return currClient;
            }
        }
        return null;
    }

    /**
     * Returns the number of clients connected
     * 
     * @return Number of clients connected
     */
    public int nrOfClients() {
        return this.clients.size();
    }

}
