package no.ntnu.imt3281.ludo.server;

import java.util.ArrayList;

import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * Handles disconnects of a client. Should remove the user from all chat rooms
 * and games.
 * 
 * @author Hakkon
 *
 */
public class ClientDisconnectHandler implements ClientListener {
    private ClientManager clientManager;
    private ChatManager chatManager;
    private GameManager gameManager;

    /**
     * Constructor for disconnect handler. Gets a reference to clientManager so
     * client can be removed from the list and ChatManager so client can be removed
     * from all chat rooms.
     * 
     * @param clientManager
     *            Reference to clientManager object
     * @param chatManager
     *            Reference to chatManager object
     * @param gameManager
     *            Reference to gameManager object
     */
    public ClientDisconnectHandler(ClientManager clientManager, ChatManager chatManager, GameManager gameManager) {
        this.clientManager = clientManager;
        this.chatManager = chatManager;
        this.gameManager = gameManager;
    }

    /**
     * Is fired when a client disconnects, with a reference to the client. Client is
     * removed from all chat rooms and games they are connected in.
     */
    @Override
    public void clientDisconnected(ClientThread client) {

        // Client has logged in so remove from chats and games
        if (client.getUser() != null) {

            // Remove client from all chat rooms
            ArrayList<String> removedFrom = (ArrayList<String>) this.chatManager.removeUser(client.getUser().getName());

            // Notify clients
            for (int i = 0; i < this.clientManager.nrOfClients(); i++) {
                ClientThread notify = this.clientManager.getClient(i);
                for (String chatID : removedFrom) {
                    notify.sendCommand("ChatUsrEvent&" + chatID + "&" + client.getUser().getName() + "&LEAVE");
                }
            }

            this.gameManager.removeUserFromGames(client);

        }

        this.clientManager.removeClient(client);
    }

}
