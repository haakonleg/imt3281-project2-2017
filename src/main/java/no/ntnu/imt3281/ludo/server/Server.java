package no.ntnu.imt3281.ludo.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.command.ClientCommandInvoker;
import no.ntnu.imt3281.ludo.common.Chat;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.LudoLogger;

/**
 * 
 * This is the main class for the server. **Note, change this to extend other
 * classes if desired.**
 * 
 * @author
 *
 */
public class Server {
    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());
    
    // Port to listen on
    public static final int PORT = 65194;

    private ServerSocket serverSocket;

    private Database database;
    private ClientManager clientManager;
    private ChatManager chatManager;
    private GameManager gameManager;

    /**
     * Constructor for server. Creates the listening socket and instances of
     * required objects such as Database, ClientManager, ChatManager and more.
     * 
     * @throws IOException Thrown if server is unable to open socket
     * @throws ClassNotFoundException Thrown if the JDBC driver for database was not found
     * @throws SQLException Thrown if database could not be created
     * @throws URISyntaxException Thrown if database could not read files
     */
    public Server() throws IOException, ClassNotFoundException, SQLException, URISyntaxException {
        this.serverSocket = new ServerSocket(PORT);

        this.database = new Database();

        this.chatManager = new ChatManager();
        this.chatManager.addChat(new Chat("Global Chat"));

        this.clientManager = new ClientManager(this.serverSocket);
        this.gameManager = new GameManager(this.clientManager, this.chatManager);
        this.clientManager.setCommandInvoker(new ClientCommandInvoker(this.database.getConnection(), this.chatManager,
                this.clientManager, this.gameManager));
        this.clientManager.setDisconnectHandler(new ClientDisconnectHandler(this.clientManager, this.chatManager, this.gameManager));

        this.clientManager.start();
    }

    private void stopServer() {
        try {
            this.gameManager.stopRandomGameStarter();
            this.database.saveChat(this.chatManager);
            this.database.close();
            this.clientManager.close();
        } catch (IOException | SQLException e) {
            LOGGER.log(Level.WARNING, "Server was not shut down correctly", e);
        }
        LOGGER.info("Server stopped");
    }

    /**
     * Main method for server
     * @param args No arguments are used
     */
    public static void main(String[] args) {
        Server server = null;
        LudoLogger.setup("ServerLog.txt");
        try {
            server = new Server();
            LOGGER.info("Press Q+Enter to quit server");

            char ch = 0;
            Scanner in = new Scanner(System.in, "UTF-8");
            while (ch != 'Q') {
                ch = Character.toUpperCase((char) in.next().charAt(0));
            }

            in.close();
            server.stopServer();

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Unable to open server socket on port" + PORT, e);
        } catch (ClassNotFoundException | SQLException | URISyntaxException e) {
            LOGGER.log(Level.SEVERE, "Unable to connect to database", e);
        }
        
        System.exit(0);
    }

}
