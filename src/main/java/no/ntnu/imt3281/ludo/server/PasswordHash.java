package no.ntnu.imt3281.ludo.server;

import java.io.UnsupportedEncodingException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is responsible for hashing passwords
 * 
 * @author Hakkon
 *
 */
public class PasswordHash {
    private static final Logger LOGGER = Logger.getLogger(PasswordHash.class.getName());
    
    private static final byte[] salt = new byte[] { 58, 7, -32, 58, -14, 81, -11, 69 };

    private MessageDigest md;

    /**
     * The constructor. Does not do much
     */
    public PasswordHash() {
        this.md = null;
    }

    /**
     * This method hashes a password with a salt and returns the hashed result in
     * string format
     * 
     * @param password
     *            Password to hash
     * @return The hashed password
     */
    public String getHashedPassword(String password) {
        try {
            this.md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            LOGGER.log(Level.SEVERE, "No such MessageDigest algorithm", e);
        }

        this.md.update(PasswordHash.salt);
        byte[] phash = null;
        
        try {
            phash = md.digest(password.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            LOGGER.log(Level.SEVERE, "Unsopported encoding", e);
        }
        
        StringBuilder output = new StringBuilder();
        for (byte currByte : phash) {
            output.append(Integer.toString((currByte & 0xff) + 0x100, 16).substring(1));
        }
        return output.toString();
    }
}
