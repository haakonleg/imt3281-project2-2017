package no.ntnu.imt3281.ludo.server;

import java.io.IOException;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.common.Chat;
import no.ntnu.imt3281.ludo.common.ChatManager;
import no.ntnu.imt3281.ludo.common.ChatMessage;

/**
 * This class is responsible for connecting to the SQL database, and creating
 * the database if it does not already exist
 * 
 * @author Hakkon
 *
 */
public class Database {
    private static final Logger LOGGER = Logger.getLogger(Database.class.getName());
    
    private Properties settings;

    private final String dbDriver;
    private final String dbServer;
    private final String dbUser;
    private final String dbPass;

    /**
     * Constructor for database. Initializes the database by fetching the properties
     * file to get which JDBC driver to use, and username and password for the
     * database. Then it loads the driver.
     * 
     * @throws SQLException
     *             Thrown if database could not be created
     * @throws ClassNotFoundException
     *             Thrown if JDBC driver could not be loaded
     * @throws IOException
     *             Thrown if reading settings file failed
     * @throws URISyntaxException
     *             Thrown if settings file could not be found
     */
    public Database() throws SQLException, ClassNotFoundException, IOException, URISyntaxException {
        this.settings = new Properties();

        InputStream in = this.getClass().getResourceAsStream("database.properties");
        settings.load(in);
        in.close();

        this.dbDriver = settings.getProperty("db.driver");
        this.dbServer = settings.getProperty("db.server");
        this.dbUser = settings.getProperty("db.user");
        this.dbPass = settings.getProperty("db.pass");

        Class.forName(dbDriver);
        this.createDB();
    }

    private void createDB() throws SQLException, IOException, URISyntaxException {
        try (Connection newDB = DriverManager.getConnection(dbServer, dbUser, dbPass);
                Statement stmt = newDB.createStatement();) {

            String sql = new String(Files.readAllBytes(Paths.get(this.getClass().getResource("database.sql").toURI())),
                    "UTF-8");
            stmt.executeUpdate(sql);

        }
    }

    /**
     * Returns a connection handle to the database
     * 
     * @return Connection object is returned if a connection could be made. Null is
     *         returned if connection could not be made.
     */
    public Connection getConnection() {
        try {
            return DriverManager.getConnection(dbServer, dbUser, dbPass);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Error connecting to database", e);
        }
        return null;
    }

    /**
     * Closes the database by sending query "SHUTDOWN".
     * 
     * @throws SQLException
     *             Thrown if error shutting down database
     */
    public void close() throws SQLException {
        try (Statement stmt = this.getConnection().createStatement()) {
            stmt.execute("SHUTDOWN");
        }
    }

    /**
     * This method stores every chat room and chat message in the database.
     * 
     * @param cManager
     *            A reference to the ChatManager object
     * @throws SQLException
     *             Thrown if there was an error saving chat in database
     */
    public void saveChat(ChatManager cManager) throws SQLException {
        Connection db = this.getConnection();

        Function<Chat, Boolean> save = toSave -> {
            String sql = "INSERT INTO Chat (chatID, chatName, timestamp) VALUES(?, ?, ?)";

            try (PreparedStatement stmt = db.prepareStatement(sql)) {
                stmt.setString(1, toSave.getID());
                stmt.setString(2, toSave.getName());
                stmt.setLong(3, toSave.getTimestamp());
                stmt.executeUpdate();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "Error saving chat: " + toSave.getID(), e);
                return false;
            }

            return true;
        };

        BiFunction<ChatMessage, String, Boolean> saveMessage = (toSave, chatID) -> {
            String sql = "INSERT INTO ChatMessage (chatID, username, message, timestamp) VALUES (?, ?, ?, ?)";

            try (PreparedStatement stmt = db.prepareStatement(sql)) {
                stmt.setString(1, chatID);
                stmt.setString(2, toSave.getUsername());
                stmt.setString(3, toSave.getMessage());
                stmt.setLong(4, toSave.getTimestamp());
                stmt.executeUpdate();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "Error saving chat message in chat: " + chatID, e);
                return false;
            }
            return true;
        };

        // Save all chat and chat messages
        for (int i = 0; i < cManager.nrOfChats(); i++) {
            Chat chat = cManager.getChat(i);

            if (!save.apply(chat)) {
                continue;
            }

            for (int j = 0; j < chat.nrOfMessages(); j++) {
                saveMessage.apply(chat.getMessage(i), chat.getID());
            }
        }

        db.close();
    }
}
