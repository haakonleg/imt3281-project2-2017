package no.ntnu.imt3281.ludo.server;

import java.util.EventListener;

import no.ntnu.imt3281.ludo.common.ClientThread;

/**
 * EventListener for client object
 * 
 * @author Hakkon
 *
 */
public interface ClientListener extends EventListener {
    /**
     * Method which is called when a client disconnects
     * @param client The client which disconnected
     */
    public void clientDisconnected(ClientThread client);
}
