package no.ntnu.imt3281.ludo.common;

import java.io.Serializable;
import java.security.SecureRandom;

/**
 * This class represents a token used for "remember me" function where the user
 * doesn't have to type in his username and password. It contains the username,
 * expiration time, and a random token which is updated on the server and client
 * each time the user logs in via "remember me".
 * 
 * @author Hakkon
 *
 */
public class RememberMeToken implements Serializable {
    private static final long serialVersionUID = 1L;

    private int userID;
    private String username;
    private long expirationTime;
    private String token;

    /**
     * Empty constructor. Use newToken method.
     */
    public RememberMeToken() {

    }

    /**
     * Only use this constructor when copying an existing token.
     * 
     * @param userID The userID
     * @param username The username
     * @param exp The expiration time
     * @param token The random token
     */
    public RememberMeToken(int userID, String username, long exp, String token) {
        this.userID = userID;
        this.username = username;
        this.expirationTime = exp;
        this.token = token;
    }

    /**
     * Static factory method for creating a new token.
     * 
     * @param userid
     *            The user ID this token is created for.
     * @param username
     *            The username this token is created for.
     * @param validFor
     *            How long this token will be valid for in hours.
     * @return The created RememberMeToken object
     */
    public static RememberMeToken newToken(int userid, String username, long validFor) {
        RememberMeToken token = new RememberMeToken();
        token.userID = userid;
        token.username = username;
        token.expirationTime = (System.currentTimeMillis() / 1000L) + (validFor * 3600L);
        token.token = RememberMeToken.generateToken(64);
        return token;
    }

    /**
     * Returns the random token used by authentication
     * 
     * @return The 64 character length random token in string format
     */
    public String getToken() {
        return this.token;
    }

    /**
     * Returns the expiration time in Unix time (seconds)
     * 
     * @return Unix time of type long
     */
    public long getExpiration() {
        return this.expirationTime;
    }

    /**
     * Returns the user ID of the user this token is associated with
     * 
     * @return user ID
     */
    public int getUser() {
        return this.userID;
    }

    /**
     * Returns the username of the user this token is associated with
     * 
     * @return username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Returns whether or not this token has expired by comparing the expirationTime
     * variable to current system time
     * 
     * @return True if this token has expired
     */
    public boolean hasExpired() {
        return this.expirationTime < System.currentTimeMillis() / 1000L;
    }

    // Generates random token
    private static String generateToken(int len) {
        final String symbols = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < len; i++) {
            sb.append(symbols.charAt(random.nextInt(symbols.length())));
        }
        return sb.toString();
    }

}
