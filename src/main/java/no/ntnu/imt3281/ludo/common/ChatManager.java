package no.ntnu.imt3281.ludo.common;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for managing all chat rooms currently active on the
 * server It holds a reference to all chat rooms in an arraylist and contains
 * methods to add and remove chat rooms.
 * 
 * @author Hakkon
 *
 */
public class ChatManager {
    // All chat rooms
    private ArrayList<Chat> chatRooms;

    /**
     * Creates a new arraylist of chat rooms
     */
    public ChatManager() {
        this.chatRooms = new ArrayList<>();
    }

    /**
     * Return a chat room by its ID
     * 
     * @param chatID
     *            The chat room ID
     * @return Chat the chat object, null if it was not found
     */
    public Chat getChat(String chatID) {
        for (Chat currChat : this.chatRooms) {
            if (currChat.getID().equals(chatID)) {
                return currChat;
            }
        }
        return null;
    }

    /**
     * Return a chat room by its integer index
     * 
     * @param index
     *            The array index
     * @return Chat the chat object
     */
    public Chat getChat(int index) {
        return this.chatRooms.get(index);
    }

    /**
     * Adds a chat room
     * 
     * @param toAdd
     *            The Chat object to add
     * @return String with the ChatID that was added, null if it was not added
     */
    public String addChat(Chat toAdd) {
        if (this.getChat(toAdd.getID()) == null) {
            this.chatRooms.add(toAdd);
            return toAdd.getID();
        }
        return null;
    }

    /**
     * Removes a chat room
     * 
     * @param chatID
     *            The room with the chatID to remove
     * @return True if it was removed
     */
    public boolean removeChat(String chatID) {
        Chat toRemove = this.getChat(chatID);
        if (toRemove != null) {
            this.chatRooms.remove(toRemove);
            return true;
        }
        return false;
    }

    /**
     * Removes a user and returns an arraylist containing the ID of the chat rooms
     * this user was deleted from
     * 
     * @param user
     *            The username of the user to delete
     * @return List of ID's of chat the user was deleted from
     */
    public List<String> removeUser(String user) {
        ArrayList<String> removedFrom = new ArrayList<>();
        for (Chat curr : this.chatRooms) {
            if (curr.hasUser(user)) {
                curr.removeUser(user);
                removedFrom.add(curr.getID());
            }
        }
        return removedFrom;
    }

    /**
     * Return number of chat rooms
     * 
     * @return int Number of chat rooms
     */
    public int nrOfChats() {
        return this.chatRooms.size();
    }
}
