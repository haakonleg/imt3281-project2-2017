package no.ntnu.imt3281.ludo.common;

import java.util.EventObject;

/**
 * Event object which is sent when a user sends a chat message
 * 
 * @author Hakkon
 *
 */
public class ChatMessageEvent extends EventObject {
    private static final long serialVersionUID = 1L;

    private String username;
    private String messageContent;
    private long timestamp;

    /**
     * Constructor
     * 
     * @param source
     *            Object source of the chat message
     */
    public ChatMessageEvent(Object source) {
        super(source);
    }

    /**
     * Constructor
     * 
     * @param source
     *            Object source of which the message originates
     * @param user
     *            The user which sent the chat message
     * @param msg
     *            The chat message contents
     * @param time
     *            Timestamp of the chat message
     */
    public ChatMessageEvent(Object source, String user, String msg, long time) {
        super(source);
        this.username = user;
        this.messageContent = msg;
        this.timestamp = time;
    }

    /**
     * Return the user which posted the chat message
     * 
     * @return String username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Return the chat message contents
     * 
     * @return String Chat message contents
     */
    public String getMessage() {
        return this.messageContent;
    }

    /**
     * Return the chat message timestamp
     * 
     * @return long Unix timestamp
     */
    public long getTimestamp() {
        return this.timestamp;
    }
}
