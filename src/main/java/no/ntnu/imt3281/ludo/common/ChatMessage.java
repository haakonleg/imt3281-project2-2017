package no.ntnu.imt3281.ludo.common;

/**
 * This class represents a chat message
 * 
 * @author Hakkon
 *
 */
public class ChatMessage {
    private String username;
    private long timestamp;
    private String message;

    /**
     * Create the chat message
     * 
     * @param username
     *            The user who posted the message
     * @param text
     *            The chat message text
     */
    public ChatMessage(String username, String text) {
        this.username = username;
        this.message = text;

        // Get Unix time in miliseconds
        this.timestamp = System.currentTimeMillis();
    }

    /**
     * Returns the text content of this chat message
     * 
     * @return string Chat message content
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Returns the unique id of the user who sent this chat message
     * 
     * @return int UserID
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Returns the time stamp in seconds in Unix time format
     * 
     * @return long Unix time stamp
     */
    public long getTimestamp() {
        return this.timestamp;
    }
}
