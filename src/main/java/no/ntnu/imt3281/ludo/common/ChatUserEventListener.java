package no.ntnu.imt3281.ludo.common;

import java.util.EventListener;

/**
 * Event listener which is fired on a user event (user joins/leaves chat room)
 * 
 * @author Hakkon
 *
 */
public interface ChatUserEventListener extends EventListener {
    /**
     * Method which is called when a user joins a chat
     * @param username Username of the user who joined
     */
    public void userJoined(String username);

    /**
     * Method which is called when a user leaves a chat
     * @param username Username of the user who left
     */
    public void userLeft(String username);
}
