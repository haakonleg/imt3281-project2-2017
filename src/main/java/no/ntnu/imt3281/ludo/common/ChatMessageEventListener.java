package no.ntnu.imt3281.ludo.common;

import java.util.EventListener;

/**
 * Event listener which is fired when a chat message is sent
 * 
 * @author Hakkon
 *
 */
public interface ChatMessageEventListener extends EventListener {
    /**
     * Method which is called when the chat message is sent
     * @param event The chat message event
     */
    public void messageSent(ChatMessageEvent event);
}
