package no.ntnu.imt3281.ludo.common;

import java.util.Objects;

/**
 * This class represents a user
 * 
 * @author Hakkon
 *
 */
public class User {
    private int userID;
    private String username;

    /**
     * User constructor
     * 
     * @param userID
     *            The user ID, this should be the auto increment ID from database
     * @param username
     *            Username of the user
     */
    public User(int userID, String username) {
        this.userID = userID;
        this.username = username;
    }

    /**
     * Returns the username of this user
     * 
     * @return String username
     */
    public String getName() {
        return this.username;
    }

    /**
     * Returns the user ID of this user
     * 
     * @return int User ID
     */
    public int getID() {
        return this.userID;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof User)) {
            return false;
        }

        if (other == this) {
            return true;
        }

        User otherUser = (User) other;
        return this.userID == otherUser.userID && this.username.equals(otherUser.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.userID, this.username);
    }
}
