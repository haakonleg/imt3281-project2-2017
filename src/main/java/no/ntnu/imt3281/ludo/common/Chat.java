package no.ntnu.imt3281.ludo.common;

import java.util.ArrayList;
import java.util.UUID;

/**
 * This class represents a chat room. It contains the participating users, a
 * random ID that is generated (used to store the chat in database), and an
 * array of ChatMessage objects.
 * 
 */

public class Chat {
    private String chatID;
    private String chatName;
    private long timestamp;
    private ArrayList<String> users;
    private ArrayList<ChatMessage> messages;
    private ChatMessageEventListener messageListener;
    private ChatUserEventListener userListener;

    /**
     * Constructs a chat object with random ID
     * 
     * @param name The chat room name
     */
    public Chat(String name) {
        this.chatID = UUID.randomUUID().toString();
        this.chatName = name;
        this.timestamp = System.currentTimeMillis();
        this.users = new ArrayList<>();
        this.messages = new ArrayList<>();
    }
    
    /**
     * Constructs a chat object with predetermined ID
     * @param name The char room name
     * @param chatID The chatID to use for this chat room
     */
    public Chat(String name, String chatID) {
        this.chatID = chatID;
        this.chatName = name;
        this.timestamp = System.currentTimeMillis();
        this.users = new ArrayList<>();
        this.messages = new ArrayList<>();
    }

    /**
     * Returns the chat room ID
     * 
     * @return String the chat instance ID
     */
    public String getID() {
        return this.chatID;
    }

    /**
     * Returns the name of this chat room.
     * 
     * @return String the chatroom name
     */
    public String getName() {
        return this.chatName;
    }

    /**
     * Returns the timestamp in Unix time (milliseconds)
     * 
     * @return long Unix timestamp
     */
    public long getTimestamp() {
        return this.timestamp;
    }

    /**
     * Returns the username of user(i) in the chat room
     * 
     * @param index
     *            The index of the user
     * @return String the username
     */
    public String getUser(int index) {
        if (index < this.users.size()) {
            return this.users.get(index);
        } else {
            return null;
        }
    }

    /**
     * Checks whether a specified user is in this chat or not
     * 
     * @param user
     *            The username to check
     * @return Returns true if user was found
     */
    public boolean hasUser(String user) {
        for (int i = 0; i < this.nrOfUsers(); i++) {
            if (this.getUser(i).equals(user)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a user to this chat
     * 
     * @param user
     *            The user object to add
     * @return True if the user does not already exist in the chat and is added
     */
    public boolean addUser(String user) {
        if (this.users.contains(user)) {
            return false;
        } else {
            this.users.add(user);

            if (this.userListener != null) {
                this.userListener.userJoined(user);
            }

            return true;
        }
    }

    /**
     * Removes a user from this chat
     * 
     * @param user
     *            The user object to remove
     * @return True if the user exists in the chat and is removed
     */
    public boolean removeUser(String user) {
        if (this.users.remove(user)) {
            if (this.userListener != null) {
                this.userListener.userLeft(user);
            }
            return true;
        }
        return false;
    }

    /**
     * Sends a chat message
     * 
     * @param user
     *            The user object associated with the user who sends the message
     * @param message
     *            Content of the chat message
     * @return True if the user exists in the chat and the message is sent
     */
    public boolean sendMessage(String user, String message) {
        if (this.users.contains(user)) {
            ChatMessage cMessage = new ChatMessage(user, message);
            this.messages.add(cMessage);

            if (this.messageListener != null) {
                ChatMessageEvent msgEvent = new ChatMessageEvent(this, user, message, cMessage.getTimestamp());
                this.messageListener.messageSent(msgEvent);
            }
            return true;
        }
        return false;
    }

    /**
     * Returns the ChatMessage object pointed to by this index
     * 
     * @param index
     *            The ChatMessage index
     * @return ChatMessage object, null object if index is out of bounds
     */
    public ChatMessage getMessage(int index) {
        if (index < this.messages.size()) {
            return this.messages.get(index);
        } else {
            return null;
        }
    }

    /**
     * Returns the number of messages sent in this chat
     * 
     * @return Integer with number of messages
     */
    public int nrOfMessages() {
        return this.messages.size();
    }

    /**
     * Returns the number of users in this chat
     * 
     * @return Integer with number of users
     */
    public int nrOfUsers() {
        return this.users.size();
    }

    /**
     * Sets the listener to get notified on a new chat message
     * 
     * @param listener
     *            The event listener to notify
     */
    public void setMessageListener(ChatMessageEventListener listener) {
        this.messageListener = listener;
    }

    /**
     * Sets the listener to get notified on a user event
     * 
     * @param listener
     *            The event listener to notify
     */
    public void setUserListener(ChatUserEventListener listener) {
        this.userListener = listener;
    }
}
