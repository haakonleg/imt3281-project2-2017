package no.ntnu.imt3281.ludo.common;

import java.io.BufferedReader;


import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.command.Command;
import no.ntnu.imt3281.ludo.server.ClientListener;

/**
 * This class represents a client connected to the server.
 * 
 * @author Hakkon
 *
 */
public class ClientThread extends Thread {
    private static final Logger LOGGER = Logger.getLogger(ClientThread.class.getName());
    
    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;
    private Command commandInvoker;
    private ClientListener listener;
    private String response;

    // Used for waiting for response
    private Semaphore waitResponse;

    private User user;

    /**
     * Constructor for the client object.
     * 
     * @param socket
     *            Socket handle to the client
     * @param invoker
     *            A reference to the CommandInvoker object, so commands can be
     *            executed
     * @param listener
     *            A reference to the ClientListener object, so disconnects can be
     *            handled
     */
    public ClientThread(Socket socket, Command invoker) {
        this.socket = socket;
        this.commandInvoker = invoker;
        this.waitResponse = new Semaphore(0);

        try {
            this.out = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream(), StandardCharsets.UTF_8), true);
            this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream(), StandardCharsets.UTF_8));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error obtaining PrintWriter and BufferedWriter", e);
        }
    }

    /**
     * Starts a thread that listens for and executes commands from the client
     */
    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                // Read and execute commands
                String inputLine = this.in.readLine();
                if (inputLine != null) {
                    inputLine = inputLine.trim();
                }

                if (inputLine.startsWith("*Response*")) {
                    this.response = inputLine.replaceFirst("^\\*Response\\*", "");
                    this.waitResponse.release();
                } else {
                    String[] data = inputLine.split("&");
                    commandInvoker.execute(this, data);
                }
            }
        } catch (IOException | NullPointerException e) {
            LOGGER.log(Level.INFO, "Client disconnected", e);
            this.close();
            if (this.listener != null) {
                this.listener.clientDisconnected(this);
            }
        }
    }

    /**
     * Closes resources used by the client, such as the socket. Also interrupts the
     * thread.
     */
    public void close() {
        try {
            this.socket.close();
            this.out.close();
            this.in.close();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Socket was not closed properly", e);
        }
        this.interrupt();
    }

    /**
     * Sets the event listener for when the client is disconnected.
     * 
     * @param listener
     *            ClientListener The event listener to register
     */
    public void setListener(ClientListener listener) {
        this.listener = listener;
    }

    /**
     * Sends a command to the client, which is executed on the client if called a
     * ClientThread object on the server, or executed on the server if called on a
     * ClientThread object on the client
     * 
     * @param command
     *            Command to send
     */
    public void sendCommand(String command) {
        this.out.println(command);
    }

    /**
     * Like sendCommand, but waits for a response which is returned by the method.
     * Will try to wait 10 seconds for a response. If no response is detected it
     * will be assumed to be server timeout and TIMEOUT is returned.
     * 
     * @param command
     *            Command to send
     * @return Response message from server/client
     */
    public String sendCommandAndWait(String command) {
        this.waitResponse.drainPermits();
        this.out.println(command);

        String commandResponse = null;
        try {
            if (this.waitResponse.tryAcquire(10, TimeUnit.SECONDS)) {
                commandResponse = this.response;
            } else {
                commandResponse = "TIMEOUT";
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return commandResponse;
    }

    /**
     * Sends a response to the client
     * 
     * @param response
     *            Response to send
     */
    public void sendResponse(String response) {
        this.out.println("*Response*" + response);
    }

    /**
     * Retuns a reference to the User object associated with this client
     * 
     * @return User object, null if client is not logged in
     */
    public User getUser() {
        return this.user;
    }

    /**
     * Sets the user object for this client
     * 
     * @param user
     *            User object
     */
    public void setUser(User user) {
        this.user = user;
    }
}
