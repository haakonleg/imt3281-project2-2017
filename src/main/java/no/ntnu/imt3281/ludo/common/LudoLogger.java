package no.ntnu.imt3281.ludo.common;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * This class sets up the logger
 * 
 * @author Hakkon
 *
 */
public class LudoLogger {
    private static final Logger LOGGER = Logger.getLogger("");
    
    private static FileHandler file;
    private static ConsoleHandler console;

    private LudoLogger() {

    }

    /**
     * Set up the logger by adding file handler and formatter
     * 
     * @param fileName
     *            The file name to save log as
     */
    public static void setup(String fileName) {
        LogManager.getLogManager().reset();
        LOGGER.setLevel(Level.INFO);

        try {
            file = new FileHandler(fileName);
        } catch (SecurityException | IOException e) {
            LOGGER.log(Level.SEVERE, "Unable to set up logger", e);
        }

        console = new ConsoleHandler();
        console.setFormatter(new ExtremelySimpleFormatter());
        file.setFormatter(new ExtremelySimpleFormatter());
        LOGGER.addHandler(file);
        LOGGER.addHandler(console);
    }

    /**
     * Log formatter
     * 
     * @author Hakkon
     *
     */
    private static class ExtremelySimpleFormatter extends Formatter {
        @Override
        public String format(LogRecord r) {
            return "[" + r.getLevel() + "][" + r.getLoggerName() + "] " + r.getMessage() + "\n";
        }
    }
}
