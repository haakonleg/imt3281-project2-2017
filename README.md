# Prosjekt 2 IMT3281

## Deltakere
* Håkon Legernæs
* Maciek Piatkowski

## Scrum 
https://dev.imt.hig.no/tracker/projects/IM/summary
Vi bruker JIRA på HIG-serveren, om lenken fungerer ikke, er prosjektet oppført som "IMT3102_h17_mp"

## Bibliotek Brukt
* [H2](https://mvnrepository.com/artifact/com.h2database/h2)
* [Gson](https://mvnrepository.com/artifact/com.google.code.gson/gson)